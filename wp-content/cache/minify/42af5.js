!function(e,r,u){"use strict";e(u).ready(function(p){var m,g,d,c,_,t,s,e,a,n,i;r.IPANORAMA&&r.IPANORAMA.WordPressLoaderInit||(r.IPANORAMA?r.IPANORAMA.WordPressLoaderInit=!0:r.IPANORAMA={WordPressLoaderInit:!0},m=0,g=p("head"),p("body"),d="ipnrm-effects-css",c="ipnrm-fontawesome-css",_="ipnrm-googlemaps-js",n=p("<link>"),t=p("<link>"),s=p("<script>"),e=p("<script>"),a=p("<script>"),n.attr({rel:"stylesheet",type:"text/css",href:ipanorama_globals.plugin_base_url+"ipanorama.min.css?ver="+ipanorama_globals.plugin_version}).appendTo(g),(n=u.getElementsByTagName("head").item(0)).appendChild(s.get(0)),n.appendChild(e.get(0)),n.appendChild(a.get(0)),s.on("load",function(){e.attr({type:"text/javascript",src:ipanorama_globals.plugin_base_url+"ipanorama.min.js?ver="+ipanorama_globals.plugin_version})}),e.on("load",function(){a.attr({type:"text/javascript",src:ipanorama_globals.plugin_base_url+"jquery.ipanorama.min.js?ver="+ipanorama_globals.plugin_version})}),a.on("load",function(){var e;"pro"!=ipanorama_globals.plan&&(e=setInterval(function(){0!=p(".ipnrm.ipnrm-ready").length&&clearInterval(e)},3e3))}),i=p(".ipanorama").length,p(".ipanorama").each(function(n,o){var e=p(o).data("json-src");p.ajax({url:e,type:"GET",dataType:"json"}).done(function(e){var a,n,t,s;e.flags.googlemaps&&null==u.getElementById(_)&&(n=p("<script>"),m++,u.getElementsByTagName("head").item(0).appendChild(n.get(0)),n.on("load",function(){m--}),n.attr({id:_,type:"text/javascript",src:"https://maps.google.com/maps/api/js"})),e.flags.fontawesome&&null==u.getElementById(c)&&p("<link>").attr({id:c,rel:"stylesheet",type:"text/css",href:ipanorama_globals.fontawesome_url+"?ver="+ipanorama_globals.plugin_version}).appendTo(g),e.flags.effects&&null==u.getElementById(d)&&p("<link>").attr({id:d,rel:"stylesheet",type:"text/css",href:ipanorama_globals.effects_url+"?ver="+ipanorama_globals.plugin_version}).appendTo(g),e.theme&&null==u.getElementById("ipnrm-theme-"+e.theme+"-css")&&(a=p("<link>"),n=ipanorama_globals.theme_base_url+e.theme+".min.css?ver="+ipanorama_globals.version,a.attr({id:"ipnrm-theme-"+e.theme+"-css",rel:"stylesheet",type:"text/css",href:n}).appendTo(g)),e.widget&&e.widget.name&&null==u.getElementById("ipnrm-widget-"+e.widget.name+"-css")&&(t=p("<link>"),s=ipanorama_globals.widget_base_url+e.widget.name+"/"+e.widget.name+".min.css?ver="+ipanorama_globals.version,t.attr({id:"ipnrm-widget-"+e.widget.name+"-css",rel:"stylesheet",type:"text/css",href:s}).appendTo(g)),e.widget&&e.widget.name&&null==u.getElementById("ipnrm-widget-"+e.widget.name+"-js")&&(t=p("<script>"),s=ipanorama_globals.widget_base_url+e.widget.name+"/"+e.widget.name+".min.js?ver="+ipanorama_globals.version,m++,u.getElementsByTagName("head").item(0).appendChild(t.get(0)),t.on("load",function(){m--}),t.attr({id:"ipnrm-widget-"+e.widget.name+"-js",type:"text/javascript",src:s})),e.sceneTransition&&e.sceneTransition.name&&null==u.getElementById("ipnrm-scene-transition-"+e.sceneTransition.name+"-js")&&(l=p("<script>"),i=ipanorama_globals.scene_transition_base_url+e.sceneTransition.name+"/"+e.sceneTransition.name+".min.js?ver="+ipanorama_globals.version,m++,u.getElementsByTagName("head").item(0).appendChild(l.get(0)),l.on("load",function(){m--}),l.attr({id:"ipnrm-scene-transition-"+e.sceneTransition.name+"-js",type:"text/javascript",src:i}));var i,r,l=p(o).data("item-id");null==u.getElementById("ipnrm-"+l+"-main-css")&&(i=p("<link>"),r=ipanorama_globals.plugin_upload_base_url+l+"/main.css?ver="+ipanorama_globals.version,i.attr({id:"ipnrm-"+l+"-main-css",rel:"stylesheet",type:"text/css",href:r}).appendTo(g)),e.flags.customcss&&null==u.getElementById("ipnrm-"+l+"-custom-css")&&(r=p("<link>"),e=ipanorama_globals.plugin_upload_base_url+l+"/custom.css?ver="+ipanorama_globals.version,r.attr({id:"ipnrm-"+l+"-custom-css",rel:"stylesheet",type:"text/css",href:e}).appendTo(g))}).always(function(e){var a;n==i-1&&(a=setInterval(function(){ipanorama_globals.settings&&ipanorama_globals.settings.styles&&t.attr({rel:"stylesheet",type:"text/css",href:ipanorama_globals.plugin_upload_base_url+"global.css?ver="+ipanorama_globals.version}).appendTo(g),0==m&&(s.attr({type:"text/javascript",src:ipanorama_globals.plugin_base_url+"three.min.js?ver="+ipanorama_globals.plugin_version}),1)&&clearInterval(a)},500))})}))})}(jQuery,window,document);