<section class="main">

    <div class="container h-100 d-lg-flex flex-column align-content-md-center justify-content-start">
        <div class="mb-3">
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/fale-conosco-icon.png" alt="" srcset="" class="m-auto ">
        </div>
        <div class="d-md-flex">
            <div class="col-lg-7 p-0">
                <h2 class="mb-3"> <?php the_field( 'titulo_-_fale_com_a_sonhar_construtora', 'option' ); ?></h2>
                <hr class="hr--default">
                <p>
                    <?php the_field( 'descricao_-_fale_com_a_sonhar_construtora', 'option' ); ?>

                  
                </p>
            </div>
            <?php $botao_fale_com_a_sonhar_construtora = get_field( 'botao_-_fale_com_a_sonhar_construtora', 'option' ); ?>
            <?php if ( $botao_fale_com_a_sonhar_construtora ) : ?>
                <a class="col-lg-5 pl-0" href="<?php echo esc_url( $botao_fale_com_a_sonhar_construtora['url'] ); ?>" target="<?php echo esc_attr( $botao_fale_com_a_sonhar_construtora['target'] ); ?>">
                    <div class="btn home__button m-md-0 col-12">
                        <?php echo esc_html( $botao_fale_com_a_sonhar_construtora['title'] ); ?>
                    </div>
                </a>
            <?php endif; ?>
        </div>
    </div>
</section>