<section class="main">
    <div class="container h-100">
        <div class="cards-slick">
            <?php

            $args = array(
                'post_type' => 'empreendimentos',
                'post_status' => 'publish',
                'posts_per_page' => -1,
                'orderby' => 'date',
                'order' => 'ASC',
            );
            $loop = new WP_Query($args);

            while ($loop->have_posts()) : $loop->the_post(); ?>
                <div class="card">
                    <?php get_template_part('loop-templates/content', 'card'); ?>
                </div>
            <?php
            endwhile;
            wp_reset_postdata();

            ?>

        </div>
    </div>
</section>