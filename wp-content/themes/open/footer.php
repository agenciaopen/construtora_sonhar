<!-- Footer -->
<footer class="main footer">
    <div class="container justify-content-center h-100">
        <div class="row  h-100">
            <div class=" col-lg-2 order-2 order-lg-1 text-center mt-3 mt-lg-0 ">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/logo.png" alt="" srcset="" class="">
            </div>
            <div class="col-lg-8 order-1 order-lg-2 ml-lg-4">
                <?php
                wp_nav_menu(array(

                    'menu_class'        => 'list-inline ',

                ));
                ?>
                <div class="d-lg-flex align-content-start">
                    <div>
                        <p class="footer__p--weigth text-center mb-2 text-lg-left p-md-0">
                            <?php the_field('titulo_-_central_de_vendas', 'option'); ?>
                        </p>

                        <p class="footer__p--size text-center text-lg-left p-md-0 m-auto m-lg-0">
                            <?php the_field('endereco', 'option'); ?>
                            <?php
                            $phone21 = get_field('telefone', 'option');
                            $phone2 = preg_replace('/\D+/', '', $phone21);

                            ?>
                            <br><br>
                            Telefone: <a href="tel:+55<?php echo $phone2; ?>" target="_blank"> <?php echo $phone21; ?></a>
                        </p>
                    </div>
                    <div class="d-flex justify-content-center justify-content-lg-start my-2 col-lg-6">
                        <?php if (have_rows('cadastro_de_redes_sociais', 'option')) : ?>
                            <?php while (have_rows('cadastro_de_redes_sociais', 'option')) : the_row(); ?>
                                <div class="p-2 footer__redes">
                                    <?php $link_da_rede_social = get_sub_field('link_da_rede_social'); ?>
                                    <?php if ($link_da_rede_social) : ?>
                                        <a href="<?php echo esc_url($link_da_rede_social['url']); ?>" target="_blank<?php echo esc_attr($link_da_rede_social['target']); ?>"class="">
                                            <?php echo esc_html($link_da_rede_social['title']); ?>
                                            <?php if (get_sub_field('icone_da_rede')) : ?>
                                                <img src="<?php the_sub_field('icone_da_rede'); ?>" />
                                            <?php endif ?>
                                        </a>
                                    <?php endif; ?>
                                </div>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php
                            ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>


    </div>


</footer>


<?php wp_footer(); ?>

  <?php  if (is_singular('empreendimentos')){ ?>

<script>
  $(document).ready(function() {

     $("#galeria__slider").lightSlider({
            item: 1,
            autoWidth: false,
            slideMove: 1, // slidemove will be 1 if loop is true
            slideMargin: 10,
    
            addClass: '',
            mode: "slide",
            useCSS: true,
            cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
            easing: 'linear', //'for jquery animation',////
    
            speed: 400, //ms'
            auto: false,
            loop: false,
            slideEndAnimation: true,
            pause: 2000,
    
            keyPress: true,
            controls: true,
            prevHtml: '',
            nextHtml: '',
    
            rtl:false,
            adaptiveHeight:true,
    
            enableTouch:true,
            enableDrag:true,
            freeMove:true,
            swipeThreshold: 40,
    
            responsive : [],
        });
        $('#galeria__slider').lightGallery({
            download: false,
			share: false,
            pullCaptionUp: true,
            subHtmlSelectorRelative: true
        });
  });
	</script>
<?php     }
?>
</body>

</html>