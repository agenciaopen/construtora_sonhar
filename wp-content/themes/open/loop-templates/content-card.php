<?php if ( get_field( 'desativar_empreendimento' ) == 1 ) : ?>
<?php else : ?>
	<a href="<?php the_permalink(); ?>">
<?php endif; ?>
		<?php $url = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail'); ?>
    	<div style="background-image: url('<?php echo $url?>')" class="card-header card__bg">
		</div>
	<?php if ( get_field( 'desativar_empreendimento' ) == 1 ) : ?>
	<?php else : ?>
		</a>
	<?php endif; ?>

	<div class="card-body">
    	<h5 class="card__title"><?php the_title(); ?></h5>
  	    <p class="card-text">
    <?php
					$terms = get_the_terms($post->ID, 'cidade');
					if ($terms) :
						foreach ($terms as $t) :
							$taxonomy_prefix = 'cidade';
							$term_id = $t->term_id;
							$term_id_prefixed = $taxonomy_prefix . '_' . $term_id;
							
						endforeach;
					endif;
					?>
        Seu imóvel na melhor localização de <?php echo $t->name;?>
        <br><br>
        <u class="text-uppercase">
        
        <?php
					$terms = get_the_terms($post->ID, 'status');
					if ($terms) :
						foreach ($terms as $t) :
							$taxonomy_prefix = 'status';
							$term_id = $t->term_id;
							$term_id_prefixed = $taxonomy_prefix . '_' . $term_id;
							echo $t->name;
						endforeach;
					endif;
					?>
        </u>
    </p>
	<?php if ( get_field( 'desativar_empreendimento' ) == 1 ) : ?>
		<a href="#" class="btn card__button btn-primary col-12 pointer_events">Em breve</a>
	<?php else : ?>
		<a href="<?php the_permalink(); ?>" class="btn card__button btn-primary col-12">Conhecer imóvel</a>
	<?php endif; ?>
</div>