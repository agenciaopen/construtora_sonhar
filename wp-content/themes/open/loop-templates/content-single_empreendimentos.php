<?php

/**
 * Single post partial template.
 *
 * @package understrap
 */

if (!defined('ABSPATH')) {
	exit; // Exit if accessed directly.
}
?>
<!-- <?php post_class(); ?> id="post-<?php the_ID(); ?> -->
<section class="main first__section single">
	<?php if (wp_is_mobile()) { ?>
		<div class="single__slick order-1 order-lg-2 d-md-none">
			<?php $galeria_de_imagens_images = get_field('galeria_de_imagens'); ?>
			<?php if ($galeria_de_imagens_images) :  ?>
				<?php foreach ($galeria_de_imagens_images as $galeria_de_imagens_image) : ?>
					<a href="<?php echo esc_url($galeria_de_imagens_image['sizes']['large']); ?>">
						<img src="<?php echo esc_url($galeria_de_imagens_image['sizes']['large']); ?>" alt="<?php echo esc_attr($galeria_de_imagens_image['alt']); ?>" title="<?php echo esc_attr($galeria_de_imagens_image['alt']); ?>" class="single__img img-fluid" lazy='loading'>
					</a>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	<?php }
	?>

	<div class="container h-100 order-2 order-lg-1 mt-4">
		<div class="h-100 d-flex flex-column flex-md-row">
			<div class="col-md-5 col-lg-6 p-0">
				<h3 class="single-empreendimentos__h3 mb-md-3 col-lg-6 p-0">
					<?php
					$terms = get_the_terms($post->ID, 'status');
					if ($terms) :
						foreach ($terms as $t) :
							$taxonomy_prefix = 'status';
							$term_id = $t->term_id;
							$term_id_prefixed = $taxonomy_prefix . '_' . $term_id;
							echo $t->name;
						endforeach;
					endif;
					?>
				</h3>
				<hr class="hr--default hr--dnone p-0">
				<h1 class=" p-0"><?php echo the_title(); ?></h1>
				<p class="mt-2 mb-md-4 mt-md-1 p-0"><?php the_field('subtitulo'); ?> </p>
				<div class="separator p-0 d-md-none">
					<div class="line"></div>
					<div class="d-flex flex-wrap mb-3 mt-3 check__list p-0">
						<?php if (have_rows('cadastro_de_diferenciais')) : ?>
							<?php $count = 1;
							while (have_rows('cadastro_de_diferenciais')) : the_row(); ?>
								<p class="single__p--size text-left  text-md-center pr-3 col-6"> <span class="checklist mr-2">&#128504;</span>
									<?php the_sub_field('titulo'); ?>
								</p>
							<?php $count++;
							endwhile; ?>
						<?php else : ?>
							<?php // no rows found 
							?>
						<?php endif; ?>
					</div>
					<div class="line"></div>
				</div>
				<p class="">
					<?php
					$content = apply_filters('the_content', get_the_content());
					echo $content;
					?>
					<br class="br--dnone"><br class="br--dnone">
					<?php
					$phone = get_field('n_de_telefone', 'option');
					$phone = preg_replace('/\D+/', '', $phone);
					$message = rawurldecode(get_field('mensagem_customizada', 'option'));
					?>
					<a href="https://api.whatsapp.com/send?phone=<?php echo $phone; ?>?text=<?php echo $message; ?>" rel="external" target="_blank">
						<span class="d-none d-md-block single__span">
							Fale com um consultor<img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatsapp.png" alt="" srcset="" class="pl-2" lazy='loading'>
						</span>
					</a>
				</p>
			</div>
			<?php if (get_field('imagem_chamativa_ou_video') == 'video') : ?>
				<?php the_field('video'); ?>
			<?php else : ?>
				<img src="<?php the_field('imagem_chamativa'); ?>" class="col-lg-6 col-md-7 p-0 d-none d-md-block pl-3" lazy='loading'>
			<?php endif; ?>

		</div>
		<div class="separator p-0 d-none d-md-flex mt-5">
			<div class="line"></div>
			<div class="d-flex flex-wrap mb-3 mt-3 check__list p-0">
				<?php if (have_rows('cadastro_de_diferenciais')) : ?>
					<?php $count = 1;
					while (have_rows('cadastro_de_diferenciais')) : the_row(); ?>
						<p class="single__p--size text-left  text-md-center pr-3 col-6"> <span class="checklist mr-2">&#128504;</span>
							<?php the_sub_field('titulo'); ?>
						</p>
					<?php $count++;
					endwhile; ?>
				<?php else : ?>
					<?php // no rows found 
					?>
				<?php endif; ?>

			</div>
			<div class="line"></div>
		</div>
		<div class="single__slick order-1 order-lg-2 d-none d-md-block mt-5">
			<?php $galeria_de_imagens_images = get_field('galeria_de_imagens'); ?>
			<?php if ($galeria_de_imagens_images) :  ?>
				<?php foreach ($galeria_de_imagens_images as $galeria_de_imagens_image) : ?>
					<a href="<?php echo esc_url($galeria_de_imagens_image['sizes']['large']); ?>">
						<img src="<?php echo esc_url($galeria_de_imagens_image['sizes']['large']); ?>" alt="<?php echo esc_attr($galeria_de_imagens_image['alt']); ?>" title="<?php echo esc_attr($galeria_de_imagens_image['alt']); ?>" class="single__img img-fluid pr-1" lazy='loading'>
					</a>
				<?php endforeach; ?>
			<?php endif; ?>
		</div>
	</div>
</section>

<section class="main">
	<div class="container h-100">
		<div class="column h-100 align-items-left justify-content-left">
			<h2>PERTO DE TUDO QUE VOCÊ PRECISA </h2>
			<hr class="hr--default ">
		</div>
	</div>
	<?php //the_field( 'iframe_do_mapa' ); 
	?>

	<?php if (get_field('imagem_do_mapa')) : ?>
		<img src="<?php the_field('imagem_do_mapa'); ?>" class="img-fluid col-12 col-md-6 p-0 d-md-none" lazy='loading'>
	<?php endif ?>
	<div class="container h-100">
		<div class="d-md-flex justify-content-start">
			<?php if (get_field('imagem_do_mapa')) : ?>
				<img src="<?php the_field('imagem_do_mapa'); ?>" class="col-12 col-md-6 col-lg-9 p-0 d-none d-md-block  my-auto pr-3 pr-lg-5" lazy='loading'>
			<?php endif ?>
			<div class="row h-100 d-flex flex-lg-column justify-content-between">
				<div class="col-6 col-md-12 mt-3 mt-md-0 d-flex flex-column">
					<h5 class="single__h5--color d-md-none">Proximidades</h5>
					<div class="single__div--proximidades">
						<?php if (have_rows('cadastro_de_locais_por_perto')) : ?>
							<?php while (have_rows('cadastro_de_locais_por_perto')) : the_row(); ?>
								<div class="d-flex flex-row my-2 ml-0">
									<?php if (get_sub_field('icone')) : ?>
										<img alt="<?php the_sub_field('local'); ?>" title="<?php the_sub_field('local'); ?>" src="<?php the_sub_field('icone'); ?>" class="single__icon" lazy='loading'>
									<?php endif ?>
									<p class="single__p--size"><?php the_sub_field('local', false, false); ?></p>
								</div>
							<?php endwhile; ?>

						<?php else : ?>
							<?php // no rows found 
							?>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-6  col-md-12 mt-3 mt-md-5">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/img/seta.png" alt="" srcset="" class="d-none d-lg-block" lazy='loading'>
					<h5 class="single__h5--color">Vias de acesso</h5>
					<?php if (have_rows('cadastro_de_vias_de_acesso')) : ?>
						<?php while (have_rows('cadastro_de_vias_de_acesso')) : the_row(); ?>
							<p class="single__p--size"><?php the_sub_field('titulo'); ?></p>
						<?php endwhile; ?>
					<?php else : ?>
						<?php // no rows found 
						?>
					<?php endif; ?>

				</div>
			</div>
		</div>
	</div>
</section>

<section class="main">
	<div class="container h-100">
		<div class="d-flex flex-column flex-md-row h-100 mb-4">
			<div class=" p-0">
				<div class="col-md-7 p-0">
					<h2><?php the_field('titulo_ficha_tecnica'); ?></h2>
					<hr class="hr--default ">
					<p>
						<?php the_field('descricao_ficha_tecnica'); ?>
					</p>
				</div>
				<div class="d-flex flex-wrap mb-3 mt-3">
					<?php
					$terms = get_the_terms($post->ID, 'ficha');
					if ($terms) :
						$count = 0;

						foreach ($terms as $t) :

							$taxonomy_prefix = 'ficha';
							$term_id = $t->term_id;
							$term_id_prefixed = $taxonomy_prefix . '_' . $term_id;
					?>
							<div class="col-6 col-md-4 d-flex align-items-center p-0 my-1">
								<?php if (get_field('icone', $term_id_prefixed)) : ?>
									<img src="<?php the_field('icone', $term_id_prefixed); ?>" class="my-auto pr-2 img-fluid" />
								<?php endif ?>
								<p class="single__p--size text-left">
									<?php echo $t->name; ?>
								</p>
							</div>
					<?php $count++;
						endforeach;
					endif;
					?>
				</div>
			</div>
		</div>

	<div class="single__plantas_g mb-4" id="galeria__slider">    
		<a data-sub-html=".caption" href="<?php the_field('planta_do_imovel'); ?>">
					<?php if (get_field('planta_do_imovel')) : ?>
							<img alt="Tipologia das unidades" title="Tipologia das unidades" src="<?php the_field('planta_do_imovel'); ?>" class="col-12 p-0" lazy='loading' />
					<?php endif ?>
					<div class="caption">
						<h5>Tipologia das unidades</h5>
						<?php if (have_rows('tipologia_das_unidades')) : ?>
							<?php while (have_rows('tipologia_das_unidades')) : the_row(); ?>
								<p class="single__p--size py-1">
									<?php the_sub_field('informacao'); ?>
									<?php if (get_sub_field('area_total')) : ?>
										|
										Área total: <?php the_sub_field('area_total'); ?>
									<?php endif; ?>
								</p>
							<?php endwhile; ?>
						<?php else : ?>
							<?php // no rows found 
							?>
						<?php endif; ?>
					</div>
			</a>
            <a data-sub-html=".caption" href="<?php the_field('planta_do_imovel_varanda'); ?>">
				<?php if (get_field('planta_do_imovel_varanda')) : ?>
						<img src="<?php the_field('planta_do_imovel_varanda'); ?>" class="col-12 p-0" lazy='loading' />
				<?php endif ?>
                <div class="caption">
                    <h5>Legenda:</h5>
                    <div class="d-flex flex-wrap p-0">
                        <?php if (have_rows('legenda_da_planta')) : ?>
                            <?php while (have_rows('legenda_da_planta')) : the_row(); ?>
                                <p class="mr-3 single__p--size py-1">
                                    <span class="single__legenda"><?php the_sub_field('numero'); ?></span>
                                    <?php the_sub_field('texto_da_legenda'); ?>
                                </p>
                            <?php endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found 
                            ?>
                        <?php endif; ?>
                    </div>
			   </div>
			</a>
		</div>
		<div class="single__plantas_g mb-4 planta__slick_s d-none">
			<div class="col-12 col-lg-6 p-0 item" data-src="<?php the_field('planta_do_imovel'); ?>">
				<?php if (get_field('planta_do_imovel')) : ?>
					<div class="item2" >
						<img alt="Tipologia das unidades" title="Tipologia das unidades" src="<?php the_field('planta_do_imovel'); ?>" class="col-12 p-0" lazy='loading' />
					</div>
				<?php endif ?>
				<h5>Tipologia das unidades</h5>
				<?php if (have_rows('tipologia_das_unidades')) : ?>
					<?php while (have_rows('tipologia_das_unidades')) : the_row(); ?>
						<p class="single__p--size py-1">
							<?php the_sub_field('informacao'); ?>
							<?php if (get_sub_field('area_total')) : ?>
								|
								Área total: <?php the_sub_field('area_total'); ?>
							<?php endif; ?>
						</p>
					<?php endwhile; ?>
				<?php else : ?>
					<?php // no rows found 
					?>
				<?php endif; ?>
			</div>
			<div class="col-12 col-lg-6 p-0 item" data-src="<?php the_field('planta_do_imovel_varanda'); ?>">
				<?php if (get_field('planta_do_imovel_varanda')) : ?>
					<div class="items" >
						<img src="<?php the_field('planta_do_imovel_varanda'); ?>" class="col-12 p-0" lazy='loading' />
					</div>
				<?php endif ?>
				<h5>Legenda:</h5>
				<div class="d-flex flex-wrap p-0">
					<?php if (have_rows('legenda_da_planta')) : ?>
						<?php while (have_rows('legenda_da_planta')) : the_row(); ?>
							<p class="mr-3 single__p--size py-1">
								<span class="single__legenda"><?php the_sub_field('numero'); ?></span>
								<?php the_sub_field('texto_da_legenda'); ?>
							</p>
						<?php endwhile; ?>
					<?php else : ?>
						<?php // no rows found 
						?>
					<?php endif; ?>
				</div>
			</div>
		</div>
		<?php
		$phone = get_field('n_de_telefone', 'option');
		$phone = preg_replace('/\D+/', '', $phone);
		$message = rawurldecode(get_field('mensagem_customizada', 'option'));
		?>
		<div class="align-content-center d-flex mb-4">
			<a href="https://api.whatsapp.com/send?phone=<?php echo $phone; ?>?text=<?php echo $message; ?>" rel="external" target="_blank" class="m-auto">
				<div class="btn home__button col-12">
					Falar com nossos consultores
				</div>
			</a>
		</div>
	</div>
</section>
<section class="main gray-bg pb-5">
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h2 class="d-flex justify-content-center pb-md-5 mb-4">Tour Virtual</h2>
			<h4 class="d-flex justify-content-center mb-4">clique na seta, no canto inferior da direita, para fazer o tour</h4>
		
		

		<div id="carouselExampleIndicators" class="carousel slide carousel-fade" data-ride="carousel">
			<!-- <ol class="carousel-indicators">
				<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
				<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
			</ol> -->
			<div class="carousel-inner">
				<div class="carousel-item active">
				
					<div class="col-md-12 d-flex justify-content-center">
						<embed class="pr-1 pl-1 m-0" ><?php the_field('iframe_imagem_360_slider_1'); ?></embed>
						<embed class="pr-1 pl-1 m-0" ><?php the_field('iframe_imagem_360_slider_2'); ?></embed>
						<embed class="pr-1 pl-1 m-0" ><?php the_field('iframe_imagem_360_slider_3'); ?></embed>
					</div>
					
				</div>
				<div class="carousel-item">
					<div class="col-md-12 d-flex justify-content-center">
						<embed class="pr-1 pl-1 m-0" ><?php the_field('iframe_imagem_360_slider_4'); ?></embed>
						<embed class="pr-1 pl-1 m-0" ><?php the_field('iframe_imagem_360_slider_5'); ?></embed>
						<embed class="pr-1 pl-1 m-0" ><?php the_field('iframe_imagem_360_slider_6'); ?></embed>

					</div>
					
				</div>
				
				<div class="carousel-item">
					<div class="col-md-12 d-flex justify-content-center">
						<embed class="pr-1 pl-1 m-0" ><?php the_field('iframe_imagem_360_slider_7'); ?></embed>
						<embed class="pr-1 pl-1 m-0" ><?php the_field('iframe_imagem_360_slider_8'); ?></embed>
						<embed class="pr-1 pl-1 m-0" ><?php the_field('iframe_imagem_360_slider_9'); ?></embed>

					</div>
				</div>

				<!-- <div class="carousel-item">
					<div class="col-md-12 d-flex justify-content-center">
						<a class="d-block pr-3 pl-3"><?php //the_field('iframe_imagem_360_slider_10'); ?></a>
						<a class="d-block pr-3 pl-3"><?php //the_field('iframe_imagem_360_slider_11'); ?></a>
						<a class="d-block pr-3 pl-3"><?php //the_field('iframe_imagem_360_slider_12'); ?></a>

					</div>
				</div>  -->
				
			</div>
			<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
		
		

		<?php
			// Do something...

			// End loop.
			//endwhile;

			// No value.
			//else :
				// Do something...
			//endif; 

		?>

		
	</div>
	</div>
</div>
</section>
<section class="main">
<div class="container h-100">
	
<h2 class=" d-flex justify-content-center pb-md-5 mb-4"><?php the_field('titulo_video_empreendimento'); ?></h2>
    <div class="row justify-content-center video_emp">
    
            
    <?php

		// Check rows exists.
		if( have_rows('videos_empreendimentos') ):

		// Loop through rows.
		while( have_rows('videos_empreendimentos') ) : the_row();

	?>

	<?php

		// Load sub field value.
		$sub_value = the_sub_field('videos_sonhar');
		// Do something...

		// End loop.
		endwhile;

		// No value.
		else :
			// Do something...
		endif; 

	?>
           
       
    </div>

</div>
</section>

<?php get_template_part('templates/global/template-part', 'fale-com-a-sonhar'); ?>
