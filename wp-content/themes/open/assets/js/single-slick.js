$('.single__slick').slick({
    dots: true,
    arrows: false,
    slidesToShow: 3,
    autoplay: true,
    autoplaySpeed: 4000,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 1,
                dots: true,
            }
        },

    ]
});
 
$('.single__slick').slickLightbox({
    itemSelector        : 'a',
    navigateByKeyboard  : true
  });

  
// $('.planta__slicks').slick({
//     dots: false,
//     arrows: false,
//     slidesToShow: 2,
//     responsive: [
//         {
//             breakpoint: 992,
//             settings: "unslick"
//         },
//     ]
// });
//         $(".single__plantas_g").lightSlider({
//             item: 1,
//             autoWidth: false,
//             slideMove: 1, // slidemove will be 1 if loop is true
//             slideMargin: 10,
    
//             addClass: '',
//             mode: "slide",
//             useCSS: true,
//             cssEasing: 'ease', //'cubic-bezier(0.25, 0, 0.25, 1)',//
//             easing: 'linear', //'for jquery animation',////
    
//             speed: 400, //ms'
//             auto: false,
//             loop: false,
//             slideEndAnimation: true,
//             pause: 2000,
    
//             keyPress: true,
//             controls: true,
//             prevHtml: '',
//             nextHtml: '',
    
//             rtl:false,
//             adaptiveHeight:false,
    
//             enableTouch:true,
//             enableDrag:true,
//             freeMove:true,
//             swipeThreshold: 40,
    
//             responsive : [],
//         });
// // $('.planta__slick').slickLightbox({
// //     itemSelector        : 'a',
// //     navigateByKeyboard  : true
// //   });
//       $('.single__plantas_g').lightGallery({
//             download: false,
//             pullCaptionUp: true,
//             subHtmlSelectorRelative: true
//         });