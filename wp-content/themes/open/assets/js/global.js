var header = $("#nav_main");
$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    if (scroll >= 50) {
        header.addClass("scrolled");
    } else {
        header.removeClass("scrolled");
    }
});

/*==============================================================
        navbar fixed top
        ==============================================================*/
// Hide Header on on scroll down
var didScroll;
var lastScrollTop = 0;
var delta = 100;
var navbarHeight = $('#nav_main').outerHeight();

$(window).scroll(function (event) {
    didScroll = true;
});

setInterval(function () {
    if (didScroll) {
        hasScrolled();
        didScroll = false;
    }
}, 250);

function hasScrolled() {
    var st = $(this).scrollTop();

    // Make sure they scroll more than delta
    if (Math.abs(lastScrollTop - st) <= delta)
        return;

    // If they scrolled down and are past the navbar, add class .nav-up.
    // This is necessary so you never see what is "behind" the navbar.
    if (st > lastScrollTop && st > navbarHeight) {
        // Scroll Down
        $('.navbar').removeClass('nav-down').addClass('nav-up');
    } else {
        // Scroll Up
        if (st + $(window).height() < $(document).height()) {
            $('.navbar').removeClass('nav-up').addClass('nav-down');
        }
    }

    lastScrollTop = st;
}
$('#accordion .collapse').on('shown.bs.collapse', function (e) {
    $(this).prev().addClass('active');
    var $card = $(this).closest('.card');
    var $open = $($(this).data('parent')).find('.collapse.show');
    var additionalOffset = 60;
    if ($card.prevAll().filter($open.closest('.card')).length !== 0) {
        additionalOffset = $open.height();
    }
    $('html,body').animate({
        scrollTop: $card.offset().top - additionalOffset
    }, 500);
});
$('#accordion .collapse').on('hidden.bs.collapse', function () {
    $(this).prev().removeClass('active');
});