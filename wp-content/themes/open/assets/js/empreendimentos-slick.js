$('.empreendimentos-slick').slick({
    dots: true,
    arrows: false,
    autoplay: true,
    autoplaySpeed: 4000,
    slidesToShow: 1,
});

$('.empreendimentos-slick-mb').slick({
    dots: true,
    arrows: false,
    autoplay: true,
    center: true,
    autoplaySpeed: 4000,
    slidesToShow: 1,
});

