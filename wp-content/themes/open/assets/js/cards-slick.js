$('.cards-slick').slick({
    dots: true,
    arrows: false,
    slidesToShow: 3,
    infinite: false,
    responsive: [
        {
            breakpoint: 767,
            settings: {
                slidesToShow: 2.5,
                dots: false,
            }
        },
        {
            breakpoint: 499,
            settings: {
                infinite: true,
                slidesToShow: 1.2,
                dots: false,
            }
        },

    ]
});