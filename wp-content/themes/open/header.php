<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="apple-mobile-web-app-title" content="<?php bloginfo('name'); ?> - <?php bloginfo('description'); ?>">
    <link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/assets/img/favicon_sonhar.png" />
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <title><?php wp_title(); ?></title>

    <?php wp_head(); ?>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':

new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],

j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=

'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);

})(window,document,'script','dataLayer','GTM-MZN6PN6');</script>
<!-- End Google Tag Manager -->
</head>


<body <?php body_class('sonhar_C'); ?> id="">

<!-- Google Tag Manager (noscript) -->

<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MZN6PN6"

height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
    <nav class="navbar navbar-expand-xl navbar-default fixed-top p-0 main" role="navigation" id="nav_main">
        
            <div class="container-fluid p-0">

                <div class="navbar__div--first col-10">
                    <a class="navbar-brand" href="<?php echo home_url(); ?>">
                        <img src='<?php the_field('logo_site', 'option') ?>' class='img-fluid navbar__img--logo col-6 col-md-3 col-lg-12' alt='<?php bloginfo('name'); ?>' title='<?php bloginfo('name'); ?>' loading='lazy'>
                    </a>

                    <?php
                    wp_nav_menu(array(
                        'theme_location'    => 'primary',
                        'depth'             => 2,
                        'container'         => 'div',
                        'container_class'   => 'collapse navbar-collapse',
                        'container_id'      => 'bs-example-navbar-collapse-1',
                        'menu_class'        => 'nav navbar-nav',
                        'fallback_cb'       => 'WP_Bootstrap_Navwalker::fallback',
                        'walker'            => new WP_Bootstrap_Navwalker(),
                    ));

                    ?>

                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <hr class="navbar__hr">
                        <p class="navbar__p--weight text-left">
                            <?php the_field('titulo_-_central_de_vendas', 'option'); ?>
                        </p>
                        <p class="navbar__p text-left col-8">
                            <?php the_field('endereco', 'option'); ?>
                            <?php
                            $phone2 = get_field('telefone', 'option');
                            $phone2 = preg_replace('/\D+/', '', $phone2);

                            ?>
                            <br><br>
                            Telefone: <a href="tel:+55<?php echo $phone2; ?>" target="_blank"> <?php echo $phone2; ?></a>

                        </p>

                    </div>

                </div>
                <div class="navbar__div--fourth col-2">
                    <div class="d-lg-block d-none col-12">
                        <a href="" class="d-none">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mensagem.png" alt="" srcset="" class="navbar__icon pr-4">
                        </a>
                        <?php
                        $phone = get_field('n_de_telefone', 'option');
                        $phone = preg_replace('/\D+/', '', $phone);
                        $message = rawurldecode(get_field('mensagem_customizada', 'option'));
                        ?>
                        <a href="https://api.whatsapp.com/send?phone=<?php echo $phone; ?>?text=<?php echo $message; ?>" rel="external" target="_blank">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatsapp-first.png" alt="" srcset="" class="navbar__icon">
                        </a>
                    </div>

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <button type="button" class="navbar-toggler collapsed col-12 navbar__button" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="icon-bar top-bar"></span>
                        <span class="icon-bar middle-bar"></span>
                        <span class="icon-bar bottom-bar"></span>
                    </button>
                </div>
            </div>
     
    </nav>

    <!-- <div class="navbar__bottom">

        <div class="" id="">
            <a href="" class="d-none">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mensagem.png" alt="" srcset="" class="navbar__icon--bottom">
            </a>
            <?php
            $phone = get_field('n_de_telefone', 'option');
            $phone = preg_replace('/\D+/', '', $phone);
            $message = rawurldecode(get_field('mensagem_customizada', 'option'));
            ?>
            <a href="https://api.whatsapp.com/send?phone=<?php echo $phone; ?>?text=<?php echo $message; ?>" rel="external" target="_blank">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/whatsapp.png" alt="" srcset="" class="navbar__icon--bottom">
            </a> -->

    </div>
    </div>