<?php

/**
 *
 * Template Name: Empreendimentos
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php $imovel_em_destaque = get_field('imovel_em_destaque', $pageID); ?>

<?php if ($imovel_em_destaque) : ?>
    <?php foreach ($imovel_em_destaque as $post) : ?>
        <?php setup_postdata($post); ?>
        <section class="main" id="empreendimentos">
            <div class="container h-100">
                <h3 class="mt-3 mb-4 text-md-center empreendimentos__h3--titulo">conheça o seu imóvel em <span>
                        <?php
                        $terms = get_the_terms($post->ID, 'cidade');
                        if ($terms) :
                            foreach ($terms as $t) :
                                $taxonomy_prefix = 'cidade';
                                $term_id = $t->term_id;
                                $term_id_prefixed = $taxonomy_prefix . '_' . $term_id;
                                echo $t->name;
                            endforeach;
                        endif;
                        ?>
                    </span></h3>
            </div>
            <!-- <div class="container-fluid p-0">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/empreendimentos-carrosel.png" alt="" srcset="" class="empreendimentos-carrosel" lazy='loading'>
            </div> -->

            <?php $galeria_de_imagens_images = get_field('galeria_de_imagens'); ?>
            <?php if ($galeria_de_imagens_images) :  ?>
                <div class="empreendimentos-slick-mb container-fluid p-0 col-md-6" id="empreendimentosMb">
                    <?php foreach ($galeria_de_imagens_images as $galeria_de_imagens_image) : ?>
                        <img src="<?php echo esc_url($galeria_de_imagens_image['sizes']['large']); ?>" alt="<?php echo esc_attr($galeria_de_imagens_image['alt']); ?>" title="<?php echo esc_attr($galeria_de_imagens_image['alt']); ?>" class="empreendimentos-carrosel img-fluid" lazy='loading'>
                    <?php endforeach; ?>
                </div>
            <?php endif; ?>

            <div class="container">
                <div class="empreendimentos--dnone justify-content-center mb-3 mt-3">
                    <div class="separator">
                        <div class="line"></div>
                        <div class="d-flex mb-3 mt-3 check__list">
                            <?php if (have_rows('cadastro_de_diferenciais')) : ?>
                                <?php $count = 1;
                                while (have_rows('cadastro_de_diferenciais')) : the_row(); ?>
                                    <p class="text-center <?php if ($count < 4) : ?> pr-3 <?php endif; ?>"> <span class="checklist mr-2">&#128504;</span>
                                        <?php the_sub_field('titulo'); ?>
                                    </p>
                                <?php $count++;
                                endwhile; ?>
                            <?php else : ?>
                                <?php // no rows found 
                                ?>
                            <?php endif; ?>
                        </div>
                        <div class="line"></div>
                    </div>
                </div>
                <div class="d-md-flex align-items-center justify-content-between">



                    <div class="col-md-5">
                        <h3 class="mt-3 text-sm-center text-md-left  empreendimentos__h3"><?php the_title(); ?></h3>
                        <p class="d-none d-md-block mt-1 empreendimentos__p--desktop mb-3"><?php the_field('frase_chamativa', false, false); ?></p>
                        <hr class="hr--default m-sm-auto m-md-0">
                        <h1 class="mt-3 text-center d-sm-block d-md-none"><?php the_field('frase_chamativa', false, false); ?></h1>
                        <p class="empreendimentos__p mt-md-3">
                            <?php echo wp_strip_all_tags(get_the_excerpt(), true); ?>
                            <br>
                            <br>
                            <?php
                            $terms = get_the_terms($post->ID, 'status');
                            if ($terms) :
                                foreach ($terms as $t) :
                                    $taxonomy_prefix = 'status';
                                    $term_id = $t->term_id;
                                    $term_id_prefixed = $taxonomy_prefix . '_' . $term_id;
                                    echo $t->name;
                                endforeach;
                            endif;
                            ?>
                        </p>
                        <a href="<?php the_permalink(); ?>" class="">
                            <div class="btn home__button col-12">
                                Conhecer o imóvel
                            </div>
                        </a>
                    </div>
                    <?php $galeria_de_imagens_images = get_field('galeria_de_imagens'); ?>
                    <?php if ($galeria_de_imagens_images) :  ?>
                        <div class="empreendimentos-slick container-fluid p-0 col-md-6">
                            <?php foreach ($galeria_de_imagens_images as $galeria_de_imagens_image) : ?>

                                <img src="<?php echo esc_url($galeria_de_imagens_image['sizes']['large']); ?>" alt="<?php echo esc_attr($galeria_de_imagens_image['alt']); ?>" title="<?php echo esc_attr($galeria_de_imagens_image['alt']); ?>" class="empreendimentos-carrosel-desktop img-fluid" lazy='loading'>

                            <?php endforeach; ?>
                        </div>
                    <?php endif; ?>
                </div>

            </div>
        </section>
    <?php endforeach; ?>
    <?php wp_reset_postdata(); ?>

<?php endif; ?>
<?php get_template_part('templates/global/template-part', 'busca'); ?>
<?php get_template_part('templates/global/template-part', 'empreendimentos'); ?>
<?php get_template_part('templates/global/template-part', 'fale-com-a-sonhar'); ?>

<?php get_footer(); ?>