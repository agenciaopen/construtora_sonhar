<?php

/**
 *
 * Template Name: Venda Seu Terreno
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php
$titulo_fale = get_field('titulo_venda_seu_terreno', $page_ID);
$descricao_fale = get_field('descricao_venda_seu_terreno', $page_ID);
if (get_field('imagem_quer_trabalhar_com_a_gente')) :
    $imagem_fale = get_field('imagem_venda_seu_terreno', $page_ID);
else :
    $imagem_fale = get_template_directory_uri() . '/assets/img/banner_site_sonhar-vt.png';
endif;
$form_fale = get_field('formulario_venda_seu_terreno', $page_ID);
?>

<section class="main contact">
    <img src="<?php echo $imagem_fale; ?>" alt="<?php echo $titulo_fale; ?>" title="<?php echo $titulo_fale; ?>" loading="lazy" srcset="" class="contact__banner" />
    <div class="container h-100">
        <div class="column h-100 d-md-flex align-items-start justify-content-between  p-0">
            <article class="col-md-5 p-0">
                <h1 class="mt-4 mt-md-0 p-0"><?php echo $titulo_fale; ?></h1>
                <!-- <hr class="hr--default"> -->
                <p class="mb-4 pt-4">
                    <?php echo $descricao_fale; ?>
                </p>
                <img src="<?php echo $imagem_fale; ?>" alt="<?php echo $titulo_fale; ?>" title="<?php echo $titulo_fale; ?>" loading="lazy" srcset="" class="contact__banner--desktop" />
            </article>
            <div class="col-md-7 p-0 pl-md-5">
                <?php echo $form_fale; ?>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>