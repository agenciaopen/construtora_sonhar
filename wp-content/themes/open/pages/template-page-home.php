<?php

/**
 *
 * Template Name: Home
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php
$titulo_home = get_field('home_titulo', $page_ID);
$subtitulo_home = get_field('home_subtitulo', $page_ID);
$texto_home = get_field('home_texto', $page_ID);
$botao_home = get_field('home_botao', $page_ID);
if (get_field('home_imagem')) :
    $imagem_home = get_field('home_imagem', $page_ID);
else :
    $imagem_home = get_template_directory_uri() . '/assets/img/home.png';
endif;
?>

<?php if (wp_is_mobile()) :
    else :
        if (get_field('home_background', $page_ID)) :
            $bg = get_field('home_background', $page_ID);
        else :
            $bg = wp_get_attachment_url(get_post_thumbnail_id($post->ID), 'thumbnail');
        endif;
    endif;
?> 


<section class="main home home__banner--bg" <?php if (wp_is_mobile()) : ?> <?php else :?> style="background-image: url('<?php echo $bg; ?>');" <?php endif;?>>
    <img src="<?php echo $imagem_home; ?>" alt="<?php echo $page_ID; ?>" title="<?php echo $titulo_home; ?>" loading="lazy" srcset="" class="home__banner--mobile" />
    <div class="container h-100 align-items-center justify-content-center">
        <div class="row h-100 align-items-center justify-content-center text-center">
            <div class="col-md-8 col-sm-10 col-lg-6">
                <h3 class="home__h2"><?php echo $subtitulo_home; ?></h3>
                <hr class="hr--default m-auto">
                <h1 class="home__title"><?php echo $titulo_home; ?></h1>
                <p class="home__text col-lg-8 m-auto">
                    <?php echo $texto_home; ?>
                </p>
                <div class="row h-100 align-items-center justify-content-center">
                    <div class="col-md-6">
                        <?php $home_botao = get_field('home_botao'); ?>
                        <?php if ($home_botao) : ?>
                            <?php foreach ($home_botao as $post) : ?>
                                <?php setup_postdata($post); ?>
                                <a href="<?php the_permalink(); ?>">
                                    <div class="btn home__button col-12">
                                        Conhecer o imóvel
                                    </div>
                                </a>
                            <?php endforeach; ?>
                            <?php wp_reset_postdata(); ?>
                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_template_part('templates/global/template-part', 'busca'); ?>
<?php get_template_part('templates/global/template-part', 'empreendimentos'); ?>

<?php
$titulo_conheca = get_field('conheca_a_sonhar_titulo', $page_ID);
$texto_conheca = get_field('conheca_a_sonhar_texto', $page_ID);
if (get_field('conheca_a_sonhar_imagem')) :
    $imagem_conheca = get_field('conheca_a_sonhar_imagem', $page_ID);
else :
    $imagem_conheca = get_template_directory_uri() . '/assets/img/home__banner--mobile.png';
endif;
?>
<?php if (wp_is_mobile()) :
    else :
        if (get_field('conheca_a_sonhar_background', $page_ID)) :
            $conheca_a_sonhar_background = get_field('conheca_a_sonhar_background', $page_ID);
        endif;

    endif;
?> 

<section class="main home--bg" id="conheca" <?php if (wp_is_mobile()) : ?> <?php else :?> style="background-image: url('<?php echo $conheca_a_sonhar_background; ?>');" <?php endif;?>>>
    <img src="wp-content/themes/open/assets/img/home__saiba-mais.png " alt="" srcset="" class="home__banner--mobile">
    <div class="container h-100 ">
        <div class="row align-items-center h-100">
            <div class="col-md-6 col-lg-4  d-md-flex flex-md-column justify-content-center  align-items-start">
                <h4 class="home__title--color mb-3"> <?php echo $titulo_conheca; ?> </h4>
                <p>
                    <?php echo $texto_conheca; ?>
                </p>
                <?php $conheca_a_sonhar_botao = get_field('conheca_a_sonhar_botao'); ?>
                <?php if ($conheca_a_sonhar_botao) : ?>
                    <a class="mt-lg-3" href="<?php echo esc_url($conheca_a_sonhar_botao['url']); ?>" target="<?php echo esc_attr($conheca_a_sonhar_botao['target']); ?>">
                        <div class="btn home__button mb-4 col-12 ">
                            <?php echo esc_html($conheca_a_sonhar_botao['title']); ?>
                        </div>
                    </a>
                <?php endif; ?>


            </div>
        </div>
      
    </div>
</section>

<?php
$titulo_mcva = get_field('mcva_logo', $page_ID);
$texto_mcva = get_field('mcva_texto', $page_ID);
$form_titulo_mcva = get_field('mcva_form_titulo', $page_ID);
if (get_field('mcva_a_sonhar_imagem')) :
    $imagem_mcva = get_field('mcva_a_sonhar_imagem', $page_ID);
else :
    $imagem_mcva = get_template_directory_uri() . '/assets/img/home__banner--mobile.png';
endif;
?>

<section class="main">
    <div class="container h-100 d-md-flex flex-md-row align-items-start">
        <div class="col">
            <?php $mcva_logo = get_field('mcva_logo'); ?>
            <?php if ($mcva_logo) : ?>
                <img src="<?php echo esc_url($mcva_logo['url']); ?>" alt="<?php echo esc_attr($mcva_logo['alt']); ?>" class="img-fluid mcva__logo"/>
            <?php endif; ?>
            <p class="home__text mb-2">
                <?php echo $texto_mcva; ?>
            </p>
            <?php $mcva_botao = get_field('mcva_botao'); ?>
            <?php if ($mcva_botao) : ?>
                <a href="<?php echo esc_url($mcva_botao['url']); ?>" target="<?php echo esc_attr($mcva_botao['target']); ?>">
                    <div class="btn home__button mb-sm-4 mb-md-0 mb-lg-0 col-12 ">
                        <?php echo esc_html($mcva_botao['title']); ?>
                    </div>
                </a>
            <?php endif; ?>
        </div>
        <div class="col" id="beneficio">
            <h3 class="mb-2 mt-3 mt-md-0"><?php echo $form_titulo_mcva; ?></h3>
            <?php the_field('mcva_form', $page_ID); ?>

        </div>
    </div>
</section>

<section class="main">
<div class="container h-100 d-flex justify-content-center">
    <div class="row">
        <!-- <div class="col-md-6"> -->
            <!-- <div class="col-md-6"> -->
                <?php the_field('videos_sonhar'); ?>
            <!-- </div> -->
        <!-- </div> -->
    </div>

</div>
</section>

<?php get_template_part('templates/global/template-part', 'fale-com-a-sonhar'); ?>
<?php get_footer(); ?>