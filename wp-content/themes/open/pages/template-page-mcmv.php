<?php

/**
 *
 * Template Name: MCMV
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php
$titulo_mcva_pg = get_field('mcva_logo', $page_ID);
$texto_mcva_pg = get_field('mcva_texto', $page_ID);
$subtitulo_mcva_pg = get_field('mcva_subtitulo', $page_ID);
$botao_mcva_pg = get_field('mcva_botao', $page_ID);


if (get_field('mcva_a_sonhar_imagem')) :
    $imagem_mcva_pg = get_field('mcva_a_sonhar_imagem', $page_ID);
else :
    $imagem_mcva_pg = get_template_directory_uri() . '/assets/img/home__banner--mobile.png';
endif;
?>

<img src="<?php echo get_template_directory_uri(); ?>/assets/img/CVA-banner.png" alt="" srcset="" class="img-fluid mcva__banner" lazy="loading">
<section class="main mcva">

    <div class="container h-100">
        <div class="row align-items-center">
            <div class="col-12 col-md-8">
                <?php if (get_field('mcva_logo')) : ?>
                    <img src="<?php the_field('mcva_logo'); ?>" class="img-fluid mcva__logo"/>
                <?php endif ?>
                <h2 class="mt-2 mb-2"><?php echo $subtitulo_mcva_pg ?></h2>
                <hr class="hr--default d-sm-none d-md-block">
                <p>
                    <?php echo $texto_mcva_pg ?>
                </p>
                <a href="/contato" class="">
                    <div class="btn home__button col-12 mt-4 col-md-6">
                        Fale com a Sonhar
                    </div>
                </a>
            </div>
            <div class="col-4">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/CVA-desktop.png" alt="" srcset="" class="img-fluid mcva__banner--desktop col-12" lazy="loading">
            </div>
        </div>


    </div>
</section>

<section class="main">
    <div class="container h-100 ">
        <div class="mb-4 d-lg-flex align-itens-center">
            <div class="col-12 col-md-8 col-lg-6 0 grafismo--um pr-md-0 p-0">
                <h2 class="mt-lg-4">como funciona</h2>
                <div class="mcva__itens">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/um.png" alt="" srcset="" class="img-fluid col-2  pl-0" lazy="loading">
                    <p class="d-flex flex-column">Simule o seu benefício para descobrir as suas condições exclusivas
                        <br>
                        <a href="/home/#beneficio">&#8594;Simular benefício</a>
                    </p>
                </div>
                <div class="mcva__itens">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/dois.png" alt="" srcset="" class="img-fluid col-2  pl-0" lazy="loading">
                    <p>Escolha o seu imóvel na Sonhar Construtora</p>
                </div>
                <div class="mcva__itens">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/tres.png" alt="" srcset="" class="img-fluid col-2  pl-0" lazy="loading">
                    <p>Reúna a documentação necessária
                        <br>
                        <a href="https://api.whatsapp.com/send?phone=<?php echo $phone; ?> <?php the_field( 'mensagem_customizada', 'option' ); ?>" rel="external" target="_blank">&#8594;Fale com nossos consultores</a>
                    </p>

                </div>
                <div class="mcva__itens">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/quatro.png" alt="" srcset="" class="img-fluid col-2  pl-0" lazy="loading">
                    <p>Enviaremos a documentação e acompanharemos a análise junto à Caixa Econômica Federal</p>
                </div>
                <div class="mcva__itens">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/cinco.png" alt="" srcset="" class="img-fluid col-2  pl-0" lazy="loading">
                    <p>Com a aprovação, a Caixa financia o seu imóvel e você realiza o seu sonho!</p>
                </div>
                <a href="/empreendimentos" class="">
                    <div class="btn home__button col-12">
                        Quero meu imóvel
                    </div>
                </a>
            </div>

            <div class="mt-4 mt-md-0  col-lg-6 pl-md-0">

                <div class="col-12 grafismo--dois pl-md-0 p-0">
                    <div class="col-12 pl-lg-5 mt-4 pb-lg-4 p-0">
                        <h2>vantagens do casa verde e amarela</h2>

                        <ul class="mcva__ul col-sm-12 col-md-9 col-lg-8 m-0 mt-3 p-0">

                            <li class="row">
                                <span class="checklist col-1 p-0 text-center">&#128504;</span>
                                <p class="col-10 col-md-11 p-0">Subsídios do Governo Federal de acordo com a faixa de renda</p>
                            </li>
                            <li class="row">
                                <span class="checklist col-1 p-0 text-center">&#128504;</span>
                                <p cclass="col-10 col-md-11 p-0">Menor taxa de juros do mercado</p>
                            </li>
                            <li class="row">
                                <span class="checklist col-1 p-0 text-center">&#128504;</span>
                                <p class="col-10 col-md-11 p-0">Prazos de pagamento maiores </p>
                            </li>
                            <li class="row">
                                <span class="checklist col-1 p-0 text-center">&#128504;</span>
                                <p class="col-10 p-0">Utilização do FGTS</p>
                            </li>
                            <li class="row">
                                <span class="checklist col-1 p-0 text-center">&#128504;</span>
                                <p class="col-10 col-md-11 p-0">Entrada facilitada </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
</section>

<?php get_template_part('templates/global/template-part', 'fale-com-a-sonhar'); ?>
<?php get_footer(); ?>