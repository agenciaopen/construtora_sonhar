<?php

/**
 *
 * Template Name: Fale Conosco
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>
<?php
$titulo_fale = get_field('titulo_fale_conosco', $page_ID);
$descricao_fale = get_field('descricao__fale_conosco', $page_ID);
if (get_field('imagem_quer_trabalhar_com_a_gente')) :
    $imagem_fale = get_field('imagem_fale_conosco', $page_ID);
else :
    $imagem_fale = get_template_directory_uri() . '/assets/img/contact.png';
endif;
$form_fale = get_field('formulario_fale_conosco', $page_ID);
?>

<section class="main contact">
    <img src="<?php echo $imagem_fale; ?>" alt="<?php echo $titulo_fale; ?>" title="<?php echo $titulo_fale; ?>" loading="lazy" srcset="" class="contact__banner" />
    <div class="container h-100">
        <div class="column h-100 d-md-flex align-items-start justify-content-between  p-0">
            <article class="col-md-5 p-0">
                <h1 class="mt-4 mt-md-0 p-0"><?php echo $titulo_fale; ?></h1>
                <hr class="hr--default">
                <p class="mb-4">
                    <?php echo $descricao_fale; ?>
                </p>
                <img src="<?php echo $imagem_fale; ?>" alt="<?php echo $titulo_fale; ?>" title="<?php echo $titulo_fale; ?>" loading="lazy" srcset="" class="contact__banner--desktop" />
            </article>
            <div class="col-md-7 p-0 pl-md-5">
                <?php echo $form_fale; ?>
            </div>
        </div>
    </div>
</section>
<hr class="hr--second--dnone">

<?php

$titulo_trabalhe =  get_field('titulo_quer_trabalhar_com_a_gente', $page_ID);
$descricao_trabalhe = get_field('descricao__quer_trabalhar_com_a_gente', $page_ID);
if (get_field('imagem_quer_trabalhar_com_a_gente', $page_ID)) :
    $imagem_trabalhe = get_field('imagem_quer_trabalhar_com_a_gente', $page_ID);
else :
    $imagem_trabalhe = get_template_directory_uri() . '/assets/img/contact__work-desktop.png';
endif;
$titulo_formulario_trabalhe = get_field('titulo_formulario', $page_ID);
$form_trabalhe = get_field('formulario_quer_trabalhar_com_a_gente', $page_ID);


?>
<section class="main">
    <div class="container d-md-flex align-items-center">
        <div class="h-100 col-md-5 p-0">
            <h2 class="mb-3"><?php echo $titulo_trabalhe; ?></h2>
            <p class="mb-3">
                <?php echo $descricao_trabalhe; ?>
            </p>
            <img src="<?php echo $imagem_trabalhe; ?>" alt="<?php echo $titulo_trabalhe; ?>" title="<?php echo $titulo_trabalhe; ?>" srcset="" class="m-0 p-0 w-100 contact__banner--desktop">
        </div>
        <img src="<?php echo $imagem_trabalhe; ?>" alt="<?php echo $titulo_trabalhe; ?>" title="<?php echo $titulo_trabalhe; ?>" srcset="" class="m-0 p-0 w-100 contact__banner">

        <div class="h-100">
            <h3 class="mb-3 mt-2 d-md-none"><?php echo $titulo_formulario_trabalhe; ?></h3>

            <div class="col-12 p-0 m-0 mt-md-4 pl-md-5" id="form_contact">
                <?php echo $form_trabalhe; ?>
            </div>
        </div>
</section>

<?php get_footer(); ?>