<?php

/**
 *
 * Template Name: Quem Somos
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<?php
$titulo_sobre = get_field('titulo_quem_somos', $page_ID);
$texto_sobre = get_field('texto_quem_somos', $page_ID);
$botao_sobre = get_field('botao_quem_somos', $page_ID);
if (get_field('foto_quem_somos')) :
    $imagem_sobre = get_field('foto_quem_somos', $page_ID);
else :
    $imagem_sobre = get_template_directory_uri() . '/assets/img/quem-somos.png';
endif;
?>

<section class="main about">

    <div class="container h-100">
        <div class="row h-100 justify-content-center text-left">
            <div class="d-md-flex flex-md-row-reverse align-items-end">
                <img src="<?php echo $imagem_sobre;  ?>" alt="" srcset="" class="img-fluid col-md-6 about__banner mt-3 mb-3 m-md-auto">
                <article class="mr-4 d-flex flex-column align-content-center col-md-6">
                    <h1 class=""><?php echo $titulo_sobre; ?></h1>
                    <hr class="hr--default">
                    <?php echo $texto_sobre; ?>
                    <?php $botao_quem_somos = get_field( 'botao_quem_somos' ); ?>
                    <?php if ( $botao_quem_somos ) : ?>
                        <a href="<?php echo esc_url( $botao_quem_somos['url'] ); ?>" target="<?php echo esc_attr( $botao_quem_somos['target'] ); ?>">
                            <div class="btn home__button col-12 mt-lg-4">
                                <?php echo esc_html( $botao_quem_somos['title'] ); ?>
                            </div>
                        </a>
                    <?php endif; ?>
                </article>
            </div>
        </div>
    </div>
</section>

<section class="main">
    <div class="container h-100">
        <div class="row h-100 align-items-center justify-content-center">
            <div class="about__valores p-0 d-md-flex flex-md-row flex-md-wrap col-11 justify-content-md-between align-content-md-center p-md-4">
                <?php if (have_rows('cadastro_de_itens')) : ?>
                    <?php while (have_rows('cadastro_de_itens')) : the_row(); ?>
                        <figure class="about__figure col-md-3 p-0 pr-md-4">
                            <?php if (get_sub_field('icone')) : ?>
                                <div class="about__img--icon"> <img src="<?php the_sub_field('icone'); ?>" /></div>
                            <?php endif ?>
                            <h4 class="mb-3 mt-3"><?php the_sub_field('titulo'); ?></h4>
                            <p class="mb-5"><?php the_sub_field('descricao'); ?></p>
                        </figure>
                    <?php endwhile; ?>
                <?php else : ?>
                   
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php

$diferenciais_bg = get_template_directory_uri() . '/assets/img/about-diferenciais.png';
$titulo_diferenciais = get_field('titulo_diferenciais', $page_ID);

?>

<section id="diferenciais" class="main about__bg" style="background-image: <?php echo $diferenciais_bg ?>" id="diferenciais">
    <div class="container h-100 ">
        <div class="row align-itens-end h-100 justify-content-start text-center text-md-left">
            <div class="col-md-7">
                <h2><?php echo $titulo_diferenciais; ?></h2>
                <hr class="hr--default m-auto m-md-0">
            </div>
            <ul class="about__ul col-md-10 col-lg-8 mt-md-4">
                <?php if (have_rows('itens_diferenciais')) : ?>
                    <?php while (have_rows('itens_diferenciais')) : the_row(); ?>
                        <li class="row">
                            <span class="checklist mr-2 ">&#128504;</span>
                            <?php the_sub_field('texto_diferenciais'); ?>
                        </li>
                    <?php endwhile; ?>
                <?php else : ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
</section>

<section class="main">
<div class="container h-100 d-flex justify-content-center">
    <div class="row">
        <!-- <div class="col-md-6"> -->
            <!-- <div class="col-md-6"> -->
                <?php the_field('videos_sonhar'); ?>
            <!-- </div> -->
        <!-- </div> -->
    </div>

</div>
</section>

<?php get_template_part('templates/global/template-part', 'fale-com-a-sonhar'); ?>

<?php get_footer(); ?>