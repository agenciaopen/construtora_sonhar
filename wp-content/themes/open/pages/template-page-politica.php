<?php

/**
 *
 * Template Name: Politica
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID

?>

<section class="content politica">
    <div class="container">
        <div class="row h-100 align-items-stretch justify-content-between">
            <nav class="col-sm-4" id=""> 
                <div class="col-md-12 p-0 sticky">

                    <h2>Índice</h2>
                    <hr class="hr--default">
                    <ul class="nav nav-pills nav-stacked" >
                        <?php if ( have_rows( 'cadastro_de_itens' ) ) : ?>
                            <?php $count = 1; while ( have_rows( 'cadastro_de_itens' ) ) : the_row(); ?>
                                <li>
                                    <a class="nav-link d-block <?php if ($count == 1): ?>  <?php endif;?>" href="#item-<?php echo $count;?>">
                                        <?php echo $count;?>. <?php the_sub_field( 'titulo' ); ?> 
                                    </a>
                                </li>
                                <?php if ( have_rows( 'cadastro_de_subitens' ) ) : ?>
                                    <?php $count_inner = 1; while ( have_rows( 'cadastro_de_subitens' ) ) : the_row(); ?>
                                        <li>
                                            <a class="nav-link d-block" href="#item-<?php echo $count;?>-<?php echo $count_inner;?>">
                                                <?php echo $count;?>.<?php echo $count_inner;?>. <?php the_sub_field( 'titulo' ); ?>
                                            </a>
                                        </li>
                                    <?php $count_inner++; endwhile; ?>
                                <?php else : ?>
                                    <?php // no rows found ?>
                                <?php endif; ?>
                            <?php $count++; endwhile; ?>
                        <?php else : ?>
                            <?php // no rows found ?>
                        <?php endif; ?>
                    </ul>
                </div>
            </nav>
            <div class="col-sm-1 custom-border d-none d-md-flex"></div>
            <div class="col-sm-7 mt-5 mt-md-0">
                <h1><?php the_title();?></h1>
                <hr class="hr--default">
                <?php if ( have_rows( 'cadastro_de_itens' ) ) : ?>
                    <?php $count = 1; while ( have_rows( 'cadastro_de_itens' ) ) : the_row(); ?>
                        <div id="item-<?php echo $count;?>">
                            <h2><?php echo $count;?>. <?php the_sub_field( 'titulo' ); ?></h2>
                            <p><?php the_sub_field( 'descricao' ); ?></p>
                            <?php if ( have_rows( 'cadastro_de_subitens' ) ) : ?>
                                <?php $count_inner = 1; while ( have_rows( 'cadastro_de_subitens' ) ) : the_row(); ?>
                                    <div class="pl-md-5" id="item-<?php echo $count;?>-<?php echo $count_inner;?>">
                                        <h2><?php echo $count;?>.<?php echo $count_inner;?>. <?php the_sub_field( 'titulo' ); ?></h2>
                                        <p><?php the_sub_field( 'descricao' ); ?></p>
                                    </div>
                                <?php $count_inner++; endwhile; ?>
                            <?php else : ?>
                                <?php // no rows found ?>
                            <?php endif; ?>
                        </div><!-- item-<?php echo $count;?> -->
                    <?php $count++; endwhile; ?>
                <?php else : ?>
                    <?php // no rows found ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>

