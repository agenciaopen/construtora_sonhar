<?php

/**
 *
 * Template Name: Resultados
 *
 */

get_header();
global $post;
$page_ID = $post->ID;
// get page ID
?>

<section class="main">
    <div class="container h-100">
        <div class="">
            <?php

            global $searchandfilter;
            $sf_current_query = $searchandfilter->get(37)->current_query();


            ?>
            <h3 class="mt-3 mb-4 text-md-center empreendimentos__h3--titulo">CONHEÇA O SEU <span>IMÓVEL</span></h3>

            <?php echo do_shortcode("[searchandfilter id='37' show='results']"); ?>
        </div>
    </div>
</section>
<section class="main">
    <div class="container h-100">


        <!-- <h2>Veja mais imóveis</h2>
        <hr class="hr--default"> -->
        <?php //get_template_part('templates/global/template-part', 'empreendimentos'); ?>
        <?php get_template_part('templates/global/template-part', 'fale-com-a-sonhar'); ?>
    </div>
</section>
<?php get_footer(); ?>