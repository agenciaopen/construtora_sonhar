<?php

/**
 * Search & Filter Pro 
 *
 * Sample Results Template
 * 
 * @package   Search_Filter
 * @author    Ross Morsali
 * @link      https://searchandfilter.com
 * @copyright 2018 Search & Filter
 * 
 * Note: these templates are not full page templates, rather 
 * just an encaspulation of the your results loop which should
 * be inserted in to other pages by using a shortcode - think 
 * of it as a template part
 * 
 * This template is an absolute base example showing you what
 * you can do, for more customisation see the WordPress docs 
 * and using template tags - 
 * 
 * http://codex.wordpress.org/Template_Tags
 *
 */

// If this file is called directly, abort.
if (!defined('ABSPATH')) {
	exit;
}

if ($query->have_posts()) {
?>

	<?php echo $query->found_posts; ?> resultados encontrados<br />
	<div class='search-filter-results-list d-flex flex-wrap col-6 align-content-center col-12 justify-content-center'>
		<?php
		while ($query->have_posts()) {
			$query->the_post();

		?>
			<div class='search-filter-result-item justify-content-center'>
				<div class="card col-12">
					<?php get_template_part('loop-templates/content', 'card'); ?>
				</div>
			</div>

		<?php
		}
		?>
	</div>
<?php
} else {
?>
	<div class='search-filter-results-list' data-search-filter-action='infinite-scroll-end'>
		<span>Fim dos resultados</span>
	</div>
<?php
}
?>