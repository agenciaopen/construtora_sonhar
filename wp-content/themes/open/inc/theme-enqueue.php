<?php

function open_enqueue() {
    wp_enqueue_style( 'slick-h2', get_template_directory_uri() . '/assets/css/slick-theme.min.css', array(), rand(111,9999), 'all'  );
    wp_enqueue_style( 'slickt-h2', get_template_directory_uri() . '/assets/css/slick.min.css', array(), rand(111,9999), 'all'  );
	wp_enqueue_style( 'global-project', get_template_directory_uri() . '/assets/css/style.min.css', array(), rand(111,9999), 'all'  );
   
    
    wp_deregister_script('jquery');
    wp_register_script( 'jquery', get_template_directory_uri() . '/assets/js/jquery.min.js', false, false, true);
    wp_enqueue_script( 'bs4-project', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'),  '', 'all' );
    wp_enqueue_script( 'global-project-js', get_template_directory_uri() . '/assets/js/global.js', array('jquery'),  rand(111,9999), 'all' );
    wp_enqueue_script('slick-h2-js', get_template_directory_uri() . '/assets/js/slick.min.js', array('jquery') , rand(111,9999), 'all' );
    wp_enqueue_script( 'cards-slick', get_template_directory_uri() . '/assets/js/cards-slick.js', array('jquery'),  rand(111,9999), 'all' );
    wp_enqueue_script( 'empreendimentos-slick', get_template_directory_uri() . '/assets/js/empreendimentos-slick.js', array('jquery'),  rand(111,9999), 'all' );
    if (is_singular('empreendimentos')){
        wp_enqueue_script( 'slick-lightbox', get_template_directory_uri() . '/assets/js/slick-lightbox.js', array('jquery'),  '', 'all' );
        wp_enqueue_style( 'slick-lightbox-css', get_template_directory_uri() . '/assets/css/slick-lightbox.css', array(), rand(111,9999), 'all'  );
        wp_enqueue_style( 'lightgallery-csss', 'https://sachinchoolur.github.io/lightslider/dist/css/lightslider.css', array(), rand(111,9999), 'all'  );

        wp_enqueue_style( 'lightgallery-css', 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.14/css/lightgallery.css', array(), rand(111,9999), 'all'  );
        wp_enqueue_script( 'lightsliderjss', 'https://sachinchoolur.github.io/lightslider/dist/js/lightslider.js', array('jquery'),  rand(111,9999), 'all' );
        wp_enqueue_script( 'lightsliderjs', 'https://cdnjs.cloudflare.com/ajax/libs/lightgallery/1.6.14/js/lightgallery-all.min.js', array('jquery'),  rand(111,9999), 'all' );
        wp_enqueue_script( 'single-slick', get_template_directory_uri() . '/assets/js/single-slick.js', array('jquery'),  rand(111,9999), 'all' );
    }
}
add_action( 'wp_enqueue_scripts', 'open_enqueue' );

add_action('wp_enqueue_scripts', 'remove_sf_scripts', 100);
function remove_sf_scripts(){
	wp_deregister_script( 'jquery-ui-datepicker' );
	wp_deregister_script( 'search-filter-plugin-build' );
	wp_deregister_script( 'search-filter-chosen-script' );
}

add_action('wp_print_styles', 'remove_sf_styles', 100);
function remove_sf_styles(){
    wp_dequeue_style('search-filter-chosen-styles');
	wp_dequeue_style( 'search-filter-plugin-styles' );
}