WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-20 18:20:39

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.21
- Server software: Apache/2.4.46 (Win64) PHP/7.3.21

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/SQUAD_SONHAR_CONTAGEM_ELDORADO_IMG_-PISCINA_R04-min-150x150.jpg
- destination: C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\SQUAD_SONHAR_CONTAGEM_ELDORADO_IMG_-PISCINA_R04-min-150x150.jpg.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 70
------------


*Trying: gd* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/SQUAD_SONHAR_CONTAGEM_ELDORADO_IMG_-PISCINA_R04-min-150x150.jpg
- destination: C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\SQUAD_SONHAR_CONTAGEM_ELDORADO_IMG_-PISCINA_R04-min-150x150.jpg.webp
- log-call-arguments: true
- quality: 70

The following options have not been explicitly set, so using the following defaults:
- default-quality: 75
- max-quality: 85
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- encoding
- metadata
- near-lossless
- skip-pngs
------------

GD Version: bundled (2.1.0 compatible)
image is true color
Quality: 70. 
Consider setting quality to "auto" instead. It is generally a better idea
gd succeeded :)

Converted image in 17 ms, reducing file size with 43% (went from 6363 bytes to 3612 bytes)
