WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-20 18:16:56

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.21
- Server software: Apache/2.4.46 (Win64) PHP/7.3.21

Stack converter ignited
Destination folder does not exist. Creating folder: C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-1024x228.png
- destination: C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-1024x228.png.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-1024x228.png
- destination: C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-1024x228.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "85" -m 6 -low_memory "C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-1024x228.png" -o "C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-1024x228.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-1024x228.png.webp.lossy.webp'
File:      C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-1024x228.png
Dimension: 1024 x 228 (with alpha)
Output:    15634 bytes Y-U-V-All-PSNR 43.63 48.35 48.26   44.71 dB
           (0.54 bpp)
block count:  intra4:        437  (45.52%)
              intra16:       523  (54.48%)
              skipped:       437  (45.52%)
bytes used:  header:            114  (0.7%)
             mode-partition:   2534  (16.2%)
             transparency:      346 (99.0 dB)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |    9452 |     287 |     256 |      76 |   10071  (64.4%)
 intra16-coeffs:  |     343 |      43 |     184 |      41 |     611  (3.9%)
  chroma coeffs:  |    1677 |      79 |     127 |      20 |    1903  (12.2%)
    macroblocks:  |      43%|       4%|      17%|      36%|     960
      quantizer:  |      19 |      15 |      11 |       8 |
   filter level:  |      17 |       7 |      23 |       6 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |   11472 |     409 |     567 |     137 |   12585  (80.5%)
Lossless-alpha compressed size: 345 bytes
  * Header size: 44 bytes, image data size: 301
  * Precision Bits: histogram=4 transform=4 cache=0
  * Palette size:   26

Success
Reduction: 74% (went from 59 kb to 15 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Trying to convert by executing the following command:
C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-1024x228.png" -o "C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-1024x228.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-1024x228.png.webp.lossless.webp'
File:      C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-1024x228.png
Dimension: 1024 x 228
Output:    52030 bytes (1.78 bpp)
Lossless-ARGB compressed size: 52030 bytes
  * Header size: 1199 bytes, image data size: 50806
  * Lossless features used: PALETTE
  * Precision Bits: histogram=4 transform=4 cache=2
  * Palette size:   254

Success
Reduction: 14% (went from 59 kb to 51 kb)

Picking lossy
cwebp succeeded :)

Converted image in 981 ms, reducing file size with 74% (went from 59 kb to 15 kb)
