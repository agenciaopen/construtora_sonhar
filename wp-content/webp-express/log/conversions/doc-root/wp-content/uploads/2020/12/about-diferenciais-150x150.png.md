WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-20 18:17:32

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.3.21
- Server software: Apache/2.4.46 (Win64) PHP/7.3.21

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-150x150.png
- destination: C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-150x150.png.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-150x150.png
- destination: C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-150x150.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "85" -m 6 -low_memory "C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-150x150.png" -o "C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-150x150.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-150x150.png.webp.lossy.webp'
File:      C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-150x150.png
Dimension: 150 x 150
Output:    1128 bytes Y-U-V-All-PSNR 45.71 49.45 51.28   46.77 dB
           (0.40 bpp)
block count:  intra4:         70  (70.00%)
              intra16:        30  (30.00%)
              skipped:        19  (19.00%)
bytes used:  header:             24  (2.1%)
             mode-partition:    267  (23.7%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |     420 |      46 |      92 |      35 |     593  (52.6%)
 intra16-coeffs:  |      42 |       0 |      15 |      13 |      70  (6.2%)
  chroma coeffs:  |      97 |      12 |      25 |      12 |     146  (12.9%)
    macroblocks:  |      45%|       6%|      15%|      34%|     100
      quantizer:  |      19 |      14 |      11 |       8 |
   filter level:  |       5 |       3 |       2 |       0 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |     559 |      58 |     132 |      60 |     809  (71.7%)

Success
Reduction: 88% (went from 9511 bytes to 1128 bytes)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' n�o � reconhecido como um comando interno
ou externo, um programa oper�vel ou um arquivo em lotes.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Trying to convert by executing the following command:
C:\workspace\construtora_sonhar\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-150x150.png" -o "C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-150x150.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\workspace\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2020\12\about-diferenciais-150x150.png.webp.lossless.webp'
File:      C:\workspace\construtora_sonhar/wp-content/uploads/2020/12/about-diferenciais-150x150.png
Dimension: 150 x 150
Output:    8770 bytes (3.12 bpp)
Lossless-ARGB compressed size: 8770 bytes
  * Header size: 666 bytes, image data size: 8078
  * Lossless features used: PALETTE
  * Precision Bits: histogram=3 transform=3 cache=1
  * Palette size:   255

Success
Reduction: 8% (went from 9511 bytes to 8770 bytes)

Picking lossy
cwebp succeeded :)

Converted image in 1262 ms, reducing file size with 88% (went from 9511 bytes to 1128 bytes)
