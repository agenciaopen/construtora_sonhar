WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-20 20:24:26

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.4.14
- Server software: Microsoft-IIS/10.0

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/uploads/2021/shutterstock_552413260-min.jpg
- destination: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/uploads/2021\shutterstock_552413260-min.jpg.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 70
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/uploads/2021/shutterstock_552413260-min.jpg
- destination: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/uploads/2021\shutterstock_552413260-min.jpg.webp
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 70
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- alpha-quality: 85
- auto-filter: false
- default-quality: 75
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' is not recognized as an internal or external command,
operable program or batch file.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Quality: 70. 
Consider setting quality to "auto" instead. It is generally a better idea
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 70 -alpha_q "85" -m 6 -low_memory "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/uploads/2021/shutterstock_552413260-min.jpg" -o "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/uploads/2021\shutterstock_552413260-min.jpg.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/uploads/2021\shutterstock_552413260-min.jpg.webp.lossy.webp'
File:      C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/uploads/2021/shutterstock_552413260-min.jpg
Dimension: 7360 x 4912
Output:    579970 bytes Y-U-V-All-PSNR 45.51 48.35 48.41   46.27 dB
           (0.13 bpp)
block count:  intra4:      35829  (25.37%)
              intra16:    105391  (74.63%)
              skipped:     37680  (26.68%)
bytes used:  header:            341  (0.1%)
             mode-partition: 198098  (34.2%)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |   83922 |   24456 |   23556 |    6859 |  138793  (23.9%)
 intra16-coeffs:  |   16429 |   21488 |   43354 |   49988 |  131259  (22.6%)
  chroma coeffs:  |   31123 |   16857 |   30096 |   33374 |  111450  (19.2%)
    macroblocks:  |      12%|       9%|      22%|      57%|  141220
      quantizer:  |      39 |      35 |      29 |      23 |
   filter level:  |      63 |      63 |      63 |      46 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |  131474 |   62801 |   97006 |   90221 |  381502  (65.8%)

Success
Reduction: 45% (went from 1026 kb to 566 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' is not recognized as an internal or external command,
operable program or batch file.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Trying to convert by executing the following command:
C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 70 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/uploads/2021/shutterstock_552413260-min.jpg" -o "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/uploads/2021\shutterstock_552413260-min.jpg.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/uploads/2021\shutterstock_552413260-min.jpg.webp.lossless.webp'
File:      C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/uploads/2021/shutterstock_552413260-min.jpg
Dimension: 7360 x 4912
Output:    7659096 bytes (1.69 bpp)
Lossless-ARGB compressed size: 7659096 bytes
  * Header size: 103643 bytes, image data size: 7555427
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM SUBTRACT-GREEN
  * Precision Bits: histogram=7 transform=4 cache=10

Success
Reduction: -629% (went from 1026 kb to 7480 kb)

Picking lossy
cwebp succeeded :)

Converted image in 59107 ms, reducing file size with 45% (went from 1026 kb to 566 kb)
