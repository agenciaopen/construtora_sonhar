WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-20 20:49:55

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.4.14
- Server software: Microsoft-IIS/10.0

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/single-video.png
- destination: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\single-video.png.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/single-video.png
- destination: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\single-video.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' is not recognized as an internal or external command,
operable program or batch file.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "85" -m 6 -low_memory "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/single-video.png" -o "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\single-video.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\single-video.png.webp.lossy.webp'
File:      C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/single-video.png
Dimension: 573 x 323 (with alpha)
Output:    20418 bytes Y-U-V-All-PSNR 43.39 45.58 45.28   43.97 dB
           (0.88 bpp)
block count:  intra4:        667  (88.23%)
              intra16:        89  (11.77%)
              skipped:        23  (3.04%)
bytes used:  header:            133  (0.7%)
             mode-partition:   3061  (15.0%)
             transparency:       33 (99.0 dB)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |   11058 |     346 |     338 |     359 |   12101  (59.3%)
 intra16-coeffs:  |     254 |      53 |      33 |      89 |     429  (2.1%)
  chroma coeffs:  |    4067 |     170 |     162 |     207 |    4606  (22.6%)
    macroblocks:  |      75%|       6%|       5%|      14%|     756
      quantizer:  |      16 |      12 |       8 |       8 |
   filter level:  |       7 |       3 |       2 |      11 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |   15379 |     569 |     533 |     655 |   17136  (83.9%)
Lossless-alpha compressed size: 32 bytes
  * Header size: 21 bytes, image data size: 11
  * Lossless features used: PALETTE
  * Precision Bits: histogram=4 transform=4 cache=0
  * Palette size:   2

Success
Reduction: 93% (went from 283 kb to 20 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' is not recognized as an internal or external command,
operable program or batch file.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Trying to convert by executing the following command:
C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/single-video.png" -o "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\single-video.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\single-video.png.webp.lossless.webp'
File:      C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/single-video.png
Dimension: 573 x 323
Output:    131400 bytes (5.68 bpp)
Lossless-ARGB compressed size: 131400 bytes
  * Header size: 2234 bytes, image data size: 129140
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM SUBTRACT-GREEN
  * Precision Bits: histogram=4 transform=4 cache=10

Success
Reduction: 55% (went from 283 kb to 128 kb)

Picking lossy
cwebp succeeded :)

Converted image in 1118 ms, reducing file size with 93% (went from 283 kb to 20 kb)
