WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-20 20:49:18

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.4.14
- Server software: Microsoft-IIS/10.0

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/icone-diferenciais13.png
- destination: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\icone-diferenciais13.png.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/icone-diferenciais13.png
- destination: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\icone-diferenciais13.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' is not recognized as an internal or external command,
operable program or batch file.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "85" -m 6 -low_memory "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/icone-diferenciais13.png" -o "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\icone-diferenciais13.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\icone-diferenciais13.png.webp.lossy.webp'
File:      C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/icone-diferenciais13.png
Dimension: 18 x 31 (with alpha)
Output:    468 bytes Y-U-V-All-PSNR 50.53 46.00 41.02   46.33 dB
           (6.71 bpp)
block count:  intra4:          0  (0.00%)
              intra16:         4  (100.00%)
              skipped:         0  (0.00%)
bytes used:  header:             47  (10.0%)
             mode-partition:      5  (1.1%)
             transparency:      232 (99.0 dB)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |       0 |       0 |       0 |       0 |       0  (0.0%)
 intra16-coeffs:  |      17 |       0 |       0 |       1 |      18  (3.8%)
  chroma coeffs:  |      67 |       0 |       0 |      41 |     108  (23.1%)
    macroblocks:  |      75%|       0%|       0%|      25%|       4
      quantizer:  |      17 |      13 |      10 |       8 |
   filter level:  |       5 |       3 |       2 |       0 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |      84 |       0 |       0 |      42 |     126  (26.9%)
Lossless-alpha compressed size: 231 bytes
  * Header size: 37 bytes, image data size: 194
  * Precision Bits: histogram=3 transform=3 cache=0
  * Palette size:   22

Success
Reduction: 47% (went from 882 bytes to 468 bytes)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' is not recognized as an internal or external command,
operable program or batch file.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Trying to convert by executing the following command:
C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/icone-diferenciais13.png" -o "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\icone-diferenciais13.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\icone-diferenciais13.png.webp.lossless.webp'
File:      C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/icone-diferenciais13.png
Dimension: 18 x 31
Output:    338 bytes (4.85 bpp)
Lossless-ARGB compressed size: 338 bytes
  * Header size: 102 bytes, image data size: 211
  * Lossless features used: PALETTE
  * Precision Bits: histogram=3 transform=3 cache=0
  * Palette size:   35

Success
Reduction: 62% (went from 882 bytes to 338 bytes)

Picking lossless
cwebp succeeded :)

Converted image in 654 ms, reducing file size with 62% (went from 882 bytes to 338 bytes)
