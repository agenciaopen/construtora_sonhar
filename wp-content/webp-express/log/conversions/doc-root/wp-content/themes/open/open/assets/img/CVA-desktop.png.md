WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-01-20 20:48:50

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.4.14
- Server software: Microsoft-IIS/10.0

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/CVA-desktop.png
- destination: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\CVA-desktop.png.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- alpha-quality: 85
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 85
------------


*Trying: cwebp* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/CVA-desktop.png
- destination: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\CVA-desktop.png.webp
- alpha-quality: 85
- encoding: "auto"
- low-memory: true
- log-call-arguments: true
- metadata: "none"
- method: 6
- near-lossless: 60
- quality: 85
- use-nice: true
- command-line-options: ""
- try-common-system-paths: true
- try-supplied-binary-for-os: true

The following options have not been explicitly set, so using the following defaults:
- auto-filter: false
- default-quality: 85
- max-quality: 85
- preset: "none"
- size-in-percentage: null (not set)
- skip: false
- rel-path-to-precompiled-binaries: *****
- try-cwebp: true
- try-discovering-cwebp: true
------------

Encoding is set to auto - converting to both lossless and lossy and selecting the smallest file

Converting to lossy
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' is not recognized as an internal or external command,
operable program or batch file.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Quality: 85. 
The near-lossless option ignored for lossy
Trying to convert by executing the following command:
C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "85" -m 6 -low_memory "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/CVA-desktop.png" -o "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\CVA-desktop.png.webp.lossy.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\CVA-desktop.png.webp.lossy.webp'
File:      C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/CVA-desktop.png
Dimension: 356 x 361 (with alpha)
Output:    16548 bytes Y-U-V-All-PSNR 43.33 42.24 42.55   42.99 dB
           (1.03 bpp)
block count:  intra4:        429  (81.10%)
              intra16:       100  (18.90%)
              skipped:         6  (1.13%)
bytes used:  header:            183  (1.1%)
             mode-partition:   2091  (12.6%)
             transparency:      343 (99.0 dB)
 Residuals bytes  |segment 1|segment 2|segment 3|segment 4|  total
  intra4-coeffs:  |    7125 |     236 |     329 |     142 |    7832  (47.3%)
 intra16-coeffs:  |     112 |      66 |      49 |     139 |     366  (2.2%)
  chroma coeffs:  |    4265 |     250 |     747 |     416 |    5678  (34.3%)
    macroblocks:  |      63%|       8%|      11%|      18%|     529
      quantizer:  |      17 |      12 |       9 |       8 |
   filter level:  |       5 |       6 |       2 |       0 |
------------------+---------+---------+---------+---------+-----------------
 segments total:  |   11502 |     552 |    1125 |     697 |   13876  (83.9%)
Lossless-alpha compressed size: 342 bytes
  * Header size: 41 bytes, image data size: 301
  * Precision Bits: histogram=3 transform=3 cache=0
  * Palette size:   21

Success
Reduction: 92% (went from 215 kb to 16 kb)

Converting to lossless
Looking for cwebp binaries.
Discovering if a plain cwebp call works (to skip this step, disable the "try-cwebp" option)
- Executing: cwebp -version 2>&1. Result: *Exec failed* (return code: 1)

*Output:* 
'cwebp' is not recognized as an internal or external command,
operable program or batch file.

Nope a plain cwebp call does not work
Discovering binaries using "which -a cwebp" command. (to skip this step, disable the "try-discovering-cwebp" option)
Found 0 binaries
Discovering binaries by peeking in common system paths (to skip this step, disable the "try-common-system-paths" option)
Found 0 binaries
Discovering binaries which are distributed with the webp-convert library (to skip this step, disable the "try-supplied-binary-for-os" option)
Checking if we have a supplied precompiled binary for your OS (WINNT)... We do.
Found 1 binaries: 
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe
Detecting versions of the cwebp binaries found
- Executing: C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -version 2>&1. Result: version: *1.1.0*
Binaries ordered by version number.
- C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe: (version: 1.1.0)
Trying the first of these. If that should fail (it should not), the next will be tried and so on.
Creating command line options for version: 1.1.0
Trying to convert by executing the following command:
C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br\wp-content\plugins\webp-express\vendor\rosell-dk\webp-convert\src\Convert\Converters\Binaries\cwebp-110-windows-x64.exe -metadata none -q 85 -alpha_q "85" -near_lossless 60 -m 6 -low_memory "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/CVA-desktop.png" -o "C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\CVA-desktop.png.webp.lossless.webp" 2>&1 2>&1

*Output:* 
Saving file 'C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/webp-express/webp-images/themes/open\open\assets\img\CVA-desktop.png.webp.lossless.webp'
File:      C:\Inetpub\vhosts\opencorporate.com\sonhar.agenciaopen.com.br/wp-content/themes/open/open/assets/img/CVA-desktop.png
Dimension: 356 x 361
Output:    96770 bytes (6.02 bpp)
Lossless-ARGB compressed size: 96770 bytes
  * Header size: 3871 bytes, image data size: 92873
  * Lossless features used: PREDICTION CROSS-COLOR-TRANSFORM SUBTRACT-GREEN
  * Precision Bits: histogram=3 transform=3 cache=10

Success
Reduction: 56% (went from 215 kb to 95 kb)

Picking lossy
cwebp succeeded :)

Converted image in 1297 ms, reducing file size with 92% (went from 215 kb to 16 kb)
