WebP Express 0.19.0. Conversion triggered using bulk conversion, 2021-09-15 13:51:24

*WebP Convert 2.3.2*  ignited.
- PHP version: 7.4.22
- Server software: Apache/2.4.48 (Win64) OpenSSL/1.1.1k PHP/7.4.22

Stack converter ignited

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\xampp\htdocs\construtora_sonhar/wp-content/uploads/2021/09/08-scaled.jpg
- destination: C:\xampp\htdocs\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2021\09\08-scaled.jpg.webp
- log-call-arguments: true
- converters: (array of 10 items)

The following options have not been explicitly set, so using the following defaults:
- converter-options: (empty array)
- shuffle: false
- preferred-converters: (empty array)
- extra-converters: (empty array)

The following options were supplied and are passed on to the converters in the stack:
- encoding: "auto"
- metadata: "none"
- near-lossless: 60
- quality: 70
------------


*Trying: gd* 

Options:
------------
The following options have been set explicitly. Note: it is the resulting options after merging down the "jpeg" and "png" options and any converter-prefixed options.
- source: C:\xampp\htdocs\construtora_sonhar/wp-content/uploads/2021/09/08-scaled.jpg
- destination: C:\xampp\htdocs\construtora_sonhar/wp-content/webp-express/webp-images/uploads/2021\09\08-scaled.jpg.webp
- log-call-arguments: true
- quality: 70

The following options have not been explicitly set, so using the following defaults:
- default-quality: 75
- max-quality: 85
- skip: false

The following options were supplied but are ignored because they are not supported by this converter:
- encoding
- metadata
- near-lossless
- skip-pngs
------------

GD Version: bundled (2.1.0 compatible)
image is true color
Quality: 70. 
Consider setting quality to "auto" instead. It is generally a better idea
gd succeeded :)

Converted image in 742 ms, reducing file size with 63% (went from 309 kb to 116 kb)
