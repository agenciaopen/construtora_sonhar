<div class="jsv-360__admin-wrapper jsv-360">
    <header class="jsv-360__swoosh-header"></header>
    <div class="jsv-360__swoosh-container">
        <div class="jsv-360__swoosh-container">
            <div style="margin-top: 0px;">
                <div class="jsv-360__backdrop">
                    <div class="jsv-360__backdrop-container">
                        <div class="jsv-360__backdrop-header">
                            <div class="jsv-360__logo-poppins"></div>
                            <div>
                                <img src="<?php
                                echo plugins_url('360-jsv/admin/img/sign-130.png', 'jsv-360') ?>" class="jsv-360__logo"
                                     alt="logo">
                            </div>
                        </div>
                        <div class="jsv-360__card">
