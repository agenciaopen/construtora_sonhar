=== Panorama ===
Contributors: abuhayat,btechnologies
Tags: panorama, 360 degree, 3d, virtual tour, krpano, 
Requires at least: 3.0
Tested up to: 5.7.1
Stable tag: 1.0.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html


A lite Weight Plugin that helps you, Easily display panoramic 360 degree images / videos into WordPress Website in Post, Page, Widget Area using shortCode. 

**[Live Demo ](http://panorama.bplugins.com/)**

= shortCode =
Follow the steps below to generate a shortCode.

1. Go to -> Panorama Viewer from Dashboard menu -> Add New Panorama.
2. Configure the viewer from Panorama Settings with your preference.
3. Copy Generated ShortCode and paste it where you like to embed the Panorama Viewer.
4. Enjoy 360 Degree Views!


= ⭐ Checkout our other WordPress Plugins- = 

🔥 **[Html5 Audio Player](https://audioplayerwp.com/)** – Best audio player plugin for WordPress.

🔥 **[Html5 Video Player](https://wpvideoplayer.com/)** – Best video player plugin for WordPress.

🔥 **[PDF Poster](http://pdfposter.com/)** – A fully-featured PDF Viewer Plugin for WordPresss.

🔥 **[StreamCast](https://wordpress.org/plugins/streamcast)** – A fully-featured Radio Player Plugin for WordPresss.

🔥 **[3D Viewer](https://3d-viewer.bplugins.com/)** – Display interactive 3D models on the webs.

== Installation ==

This section describes how to install the plugin and get it working.

e.g.

1. Upload `plugin-directory` to the `/wp-content/plugins/` directory
2. Activate the plugin through the 'Plugins' menu in WordPress
3. Use shortcode in page, post or in widgets.
4. If you want to show panorama in your theme php, Place `<?php echo do_shortcode('YOUR_SHORTCODE'); ?>` in your templates



== Changelog ==
= 1.0.3 =
* New Feature included
* Performance improved
* Fixed issues

= 1.0.2 =
* scripts updated
* New Feature included
* Performance improved
* Fixed issues

= 1.0.1 =
* Fixed mirror issues

= 1.0.0 =
* Initial Release

