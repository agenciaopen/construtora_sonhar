
;(function($) {
        $(document).ready( function() {

                const panoContainer = document.querySelectorAll('.bppiv_panorama');
                Object.values(panoContainer).map((item) => {

                        const settings = $(item).data('settings');
                        const bppivType  = settings.bppiv_type;

                        //Video options
                        if( bppivType  === 'video' ){
                                const videoeSource    = settings.bppiv_video_src ? settings.bppiv_video_src.url : [];
                                const isAutoPlay      = settings.bppiv_auto_play === '1' ? true : false;
                                const isVideoMute     = settings.bppiv_video_mute === '1' ? true : false;
                                const isVideoLoop     = settings.bppiv_video_loop === '1' ? true : false;
                                const isHideControl   = settings.control_show_hide_video === '1' ? false : true;

                                const panoramaVideo = new PANOLENS.VideoPanorama( videoeSource,
                                { 
                                        autoplay: isAutoPlay,
                                        loop: isVideoLoop,
                                        muted: isVideoMute,                                     
                                } );
                
                                const panoramaViewer = new PANOLENS.Viewer({
                                        container: item,
                                        controlBar: isHideControl,
                                });
                                panoramaViewer.add( panoramaVideo );
                                    
                        } else {

                                const imageSource     = settings.bppiv_image_src ? settings.bppiv_image_src.url : [];
                                const isAutoRotate    = settings.bppiv_auto_rotate === '1' ? true : false;
                                const autoRotateSpeed = settings.bppiv_speed ? settings.bppiv_speed : 2;
                                const isHideControl   = settings.control_show_hide === '1' ? false : true;

                                const panorama = new PANOLENS.ImagePanorama(imageSource);
                                const viewer   = new PANOLENS.Viewer({
                                        container: item,
                                        autoRotate: isAutoRotate,
                                        autoRotateSpeed: parseFloat(autoRotateSpeed),
                                        controlBar: isHideControl,
                                });
                                viewer.add( panorama )
                        }
                        
                }); 
        });
}) ( jQuery );
