<?php

/**
 * Plugin Name: Panorama
 * Description: A lite Weight Plugin that helps you, Easily display panoramic 360 degree images / videos into WordPress Website in Post, Page, Widget Area using shortCode. 
 * Plugin URI:  https://wordpress.org/plugins/
 * Version:    1.0.3
 * Author: bPlugins LLC
 * Author URI: http://abuhayatpolash.com
 * License: GPLv3
 * Text Domain: panorama-viewer
 * Domain Path:  /languages
 */

/* Plugin Initialization */
define('BPPIV_PLUGIN_DIR', WP_PLUGIN_URL . '/' . plugin_basename( dirname(__FILE__) ) . '/' ); 
define('BPPIV_PLUGIN_VERSION', '1.0.3' ); 

add_action('plugin_loaded','bppiv_textdomain');
function bppiv_textdomain(){
    load_textdomain('panorama-viewer', BPPIV_PLUGIN_DIR.'languages');

}

// Register Scripts and style
function bppiv_assets(){
    wp_register_script( 'bppiv-three', plugin_dir_url( __FILE__). 'public/js/three.min.js', null, BPPIV_PLUGIN_VERSION, false );
    wp_register_script( 'bppiv-panolens', plugin_dir_url( __FILE__ ). 'public/js/panolens.min.js', array('bppiv-three'), BPPIV_PLUGIN_VERSION, false );
    wp_register_script( 'bppiv-init', plugin_dir_url( __FILE__ ). 'public/js/scripts.js', array('jquery','bppiv-three', 'bppiv-panolens'), BPPIV_PLUGIN_VERSION, true );
}
add_action('wp_enqueue_scripts', 'bppiv_assets');


// Additional admin style
function bppiv_admin_style(){
    wp_register_style( 'bppiv-custom-style', plugin_dir_url( __FILE__ ). 'public/css/custom-style.css');
    wp_enqueue_style('bppiv-custom-style');
}
add_action('admin_enqueue_scripts', 'bppiv_admin_style');




// Shortcode for Panorama  Viewer
function bppiv_image_viewer( $atts ){
    extract( shortcode_atts( array(

            'id'    => null,

    ), $atts )); ob_start(); ?>


    <?php 
    // Meta Data
    $bppiv_meta = get_post_meta($id, '_bppivimages_', true); 

    // // Meta Options
    $bppiv_width    = '100%';
    $bppiv_height   = '320px';


    if( isset($bppiv_meta['bppiv_image_width']['width']) ) {
        $bppiv_width = $bppiv_meta['bppiv_image_width']['width'].$bppiv_meta['bppiv_image_width']['unit'];

    }
    if( isset($bppiv_meta['bppiv_image_height']['height']) ) {
        $bppiv_height = $bppiv_meta['bppiv_image_height']['height'].$bppiv_meta['bppiv_image_height']['unit'];
    }

    // Panorama Scripts and style 
    wp_enqueue_script( 'bppiv-three' );
    wp_enqueue_script( 'bppiv-panolens' );
    wp_enqueue_script( 'bppiv-init' );   

    ?>

    <div id="bppiv_panorama" class="bppiv_panorama" data-settings='<?php echo wp_json_encode( $bppiv_meta); ?>'></div>

    <style>
        .bppiv_panorama {
            width: <?php echo esc_attr($bppiv_width); ?>;
            height: <?php echo esc_attr($bppiv_height); ?>;
        }
    </style>

    <?php   
    return ob_get_clean();

}
add_shortcode( 'panorama', 'bppiv_image_viewer' );



// Custom post-type for bppiv
function bppiv_viewer(){
    $labels = array(
        'name'                  => __( 'Panorama Viewer', 'panorama-viewer' ),
        'menu_name'             => __( 'Panorama Viewer', 'panorama-viewer' ),
        'name_admin_bar'        => __( 'Panorama Viewer', 'panorama-viewer' ),
        'add_new'               => __( 'Add New Panorama', 'panorama-viewer' ),
        'add_new_item'          => __( 'Add New Panorama ', 'panorama-viewer' ),
        'new_item'              => __( 'New Panorama ', 'panorama-viewer' ),
        'edit_item'             => __( 'Edit Panorama ', 'panorama-viewer' ),
        'view_item'             => __( 'View Panorama ', 'panorama-viewer' ),
        'all_items'             => __( 'All Panoramas', 'panorama-viewer' ),
        'not_found'             => __( 'Sorry, we couldn\'t find the Feed you are looking for.' )
    ); 
    $args = array(
        'labels'             => $labels,
        'description'        => __('Panorama Options.', 'panorama-viewer'),
        'public'             => false,
        'show_ui'            => true,
        'show_in_menu'       => true,
        'menu_icon'          => 'dashicons-welcome-view-site',
        'query_var'          => true,
        'rewrite'            => array( 'slug' => 'panorama-viewer' ),
        'capability_type'    => 'post',
        'has_archive'        => false,
        'hierarchical'       => false,
        'menu_position'      => 20,
        'supports'           => array( 'title'),
    );
    register_post_type( 'bppiv-image-viewer', $args );
}
add_action( 'init', 'bppiv_viewer');
    


/*-------------------------------------------------------------------------------*/
/*   Additional Features
/*-------------------------------------------------------------------------------*/

// Hide & Disabled View, Quick Edit and Preview Button 
function bppiv_remove_row_actions( $idtions ) {
	global $post;
    if( $post->post_type == 'bppiv-image-viewer' ) {
		unset( $idtions['view'] );
		unset( $idtions['inline hide-if-no-js'] );
	}
    return $idtions;
}

if ( is_admin() ) {
add_filter( 'post_row_actions','bppiv_remove_row_actions', 10, 2 );}

// HIDE everything in PUBLISH metabox except Move to Trash & PUBLISH button
function bppiv_hide_publishing_actions(){
    $my_post_type = 'bppiv-image-viewer';
    global $post;
    if($post->post_type == $my_post_type){
        echo '
            <style type="text/css">
                #misc-publishing-actions,
                #minor-publishing-actions{
                    display:none;
                }
            </style>
        ';
    }
}
add_action('admin_head-post.php', 'bppiv_hide_publishing_actions');
add_action('admin_head-post-new.php', 'bppiv_hide_publishing_actions');	

/*-------------------------------------------------------------------------------*/
// Remove post update massage and link 
/*-------------------------------------------------------------------------------*/

function bppiv_updated_messages( $messages ) {
    $messages['bppiv-image-viewer'][1] = __('Shortcode updated ', 'panorama-viewer');
    return $messages;
}
add_filter('post_updated_messages','bppiv_updated_messages');

/*-------------------------------------------------------------------------------*/
/* Change publish button to save.
/*-------------------------------------------------------------------------------*/
add_filter( 'gettext', 'bppiv_change_publish_button', 10, 2 );
function bppiv_change_publish_button( $translation, $text ) {
if ( 'bppiv-image-viewer' == get_post_type())
if ( $text == 'Publish' )
    return 'Save';

return $translation;
}


/*-------------------------------------------------------------------------------*/
/* Footer Review Request .
/*-------------------------------------------------------------------------------*/

add_filter( 'admin_footer_text','bppiv_admin_footer');	 
function bppiv_admin_footer( $text ) {
    if ( 'bppiv-image-viewer' == get_post_type() ) {
        $url = 'https://wordpress.org/plugins/panorama-image-viewer/reviews/?filter=5#new-post';
        $text = sprintf( __( 'If you like <strong> Panorama Image Viewer </strong> please leave us a <a href="%s" target="_blank">&#9733;&#9733;&#9733;&#9733;&#9733;</a> rating. Your Review is very important to us as it helps us to grow more. ', 'panorama-viewer' ), $url );
    }

    return $text;
}

/*-------------------------------------------------------------------------------*/
/* ONLY CUSTOM POST TYPE POSTS .
/*-------------------------------------------------------------------------------*/
	
add_filter('manage_bppiv-image-viewer_posts_columns', 'bppiv_columns_head_only_panorama', 10);
add_action('manage_bppiv-image-viewer_posts_custom_column', 'bppiv_columns_content_only_panorama', 10, 2);
 
// CREATE TWO FUNCTIONS TO HANDLE THE COLUMN
function bppiv_columns_head_only_panorama($defaults) {
    $defaults['directors_name'] = 'ShortCode';
    return $defaults;
}
function bppiv_columns_content_only_panorama($column_name, $post_ID) {
    if ($column_name == 'directors_name') {
        // show content of 'directors_name' column
		echo '<input onClick="this.select();" value="[panorama id='. $post_ID . ']" >';
    }
}

/*-------------------------------------------------------------------------------*/
/* Shortcode Generator area  .
/*-------------------------------------------------------------------------------*/
	
add_action('edit_form_after_title','bppiv_shortcode_area');
function bppiv_shortcode_area(){
	global $post;	
	if($post->post_type =='bppiv-image-viewer') : ?>	
		<div class="shortcode_gen">
			<label for="bppiv_shortcode"><?php esc_html_e('Copy this shortcode and paste it into your post, page, or text widget content', 'panorama-viewer') ?>:</label>

			<span>
				<input type="text" id="bppiv_shortcode" onfocus="this.select();" readonly="readonly"  value="[panorama id=<?php echo $post->ID; ?>]" /> 		
			</span>

		</div>
	<?php endif;
}

// After activation redirect
register_activation_hook(__FILE__, 'bppiv_plugin_activate');
add_action('admin_init', 'bppiv_plugin_redirect');

function bppiv_plugin_activate() {
	add_option('bppiv_plugin_do_activation_redirect', true);
}

function bppiv_plugin_redirect() {
    if (get_option('bppiv_plugin_do_activation_redirect', false)) {
        delete_option('bppiv_plugin_do_activation_redirect');
        wp_redirect('edit.php?post_type=bppiv-image-viewer&page=bppiv-support');
    }
}




// External files Inclusion
require_once 'inc/csf/csf-config.php';
require_once 'inc/csf/metabox-options.php';
require_once 'admin/ads/submenu.php';