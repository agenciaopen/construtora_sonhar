<?php if ( ! defined( 'ABSPATH' )  ) { die; } // Cannot access directly.

//
// Metabox of the PAGE
// Set a unique slug-like ID
//
$prefix = '_bppivimages_';

//
// Create a metabox
//
CSF::createMetabox( $prefix, array(
  'title'        => 'Panorama Settings',
  'post_type'    => 'bppiv-image-viewer',
  'show_restore' => true,
  'footer_credit'    => ' ',
) );


//
// section: cPlayer Single Audio
//
CSF::createSection( $prefix, array(
  'fields' => array(

    // panorama controls
    array(
      'id'       => 'bppiv_type',
      'type'     => 'button_set',
      'title'    => esc_html__('Panorama Type.', 'panorama-viewer'),
      'subtitle' => esc_html__('Choose Panorama Type', 'panorama-viewer'),
      'desc'     => esc_html__('Select Panorama, Default- Image.', 'panorama-viewer'),
      'multiple' => false,
      'options'  => array(
        'image'   => 'Image',
        'video'   => 'Video',
      ),
      'default'  => array( 'image',)
    ),

    array(
      'id'           => 'bppiv_image_src',
      'type'         => 'media',
      'library'      => 'image',
      'button_title' => esc_html__('Upload Image', 'panorama-viewer'),
      'title'        => esc_html__('Panorama Image Source.', 'panorama-viewer'),
      'desc'         => esc_html__('To create an image panorama, an equirectangular image (2 to 1 ratio e.g. width and height is 1024 x 512) is Recommended.', 'panorama-viewer'),
      'dependency'   => array( 'bppiv_type', '==', 'image' ),
    ),
    // video source
    array(
      'id'           => 'bppiv_video_src',
      'type'         => 'media',
      'library'      => 'video',
      'button_title' => esc_html__('Upload Video', 'panorama-viewer'),
      'title'        => esc_html__('Panorama Video Source.', 'panorama-viewer'),
      'desc'         => esc_html__('Upload Panorama Video', 'panorama-viewer'),
      'dependency'   => array( 'bppiv_type', '==', 'video' ),
    ),

    array(
      'id'           => 'bppiv_image_width',
      'type'         => 'dimensions',
      'title'        => esc_html__('Panorama Width', 'panorama-viewer'),
      'desc'         => esc_html__('Panorama Viewer Width', 'panorama-viewer'),
      'default'  => array(
        'width'  => '100',
        'unit'   => '%',
      ),
      'height'   => false,
    ),
    array(
      'id'           => 'bppiv_image_height',
      'type'         => 'dimensions',
      'title'        => esc_html__('Panorama Height', 'panorama-viewer'),
      'desc'         => esc_html__('Panorama Viewer height', 'panorama-viewer'),
      'units'        => ['px', 'em', 'pt'],
      'default'  => array(
        'height' => '320',
        'unit'   => 'px',
      ),
      'width'   => false,
    ),
    array(
      'id'       => 'bppiv_auto_rotate',
      'type'     => 'switcher',
      'title'    => esc_html__('Auto Rotate ?', 'panorama-viewer'),
      'desc'     => esc_html__('Enable or Disable Auto Rotate', 'panorama-viewer'),
      'text_on'  => 'Yes',
      'text_off' => 'No',
      'default'  => true,
      'dependency'   => array( 'bppiv_type', '==', 'image' ),
    ),
    array(
      'id'       => 'bppiv_speed',
      'type'     => 'spinner',
      'title'    => esc_html__('Auto Rotate Speed', 'panorama-viewer'),
      'subtitle' => esc_html__('Choose Auto Rotate Speed', 'panorama-viewer'),
      'desc'     => esc_html__('Auto rotate speed as in degree per second. Positive is counter-clockwise and negative is clockwise.', 'panorama-viewer'),
      'default'  => 2.0,
      'dependency'   => array( 'bppiv_auto_rotate', '==', '1' ),
    ),
    array(
      'id'       => 'control_show_hide',
      'type'     => 'switcher',
      'title'    => esc_html__('Hide Control Bar ?', 'panorama-viewer'),
      'desc'     => esc_html__('Show or Hide Control', 'panorama-viewer'),
      'text_on'  => 'Yes',
      'text_off' => 'No',
      'default'  => false,
      'dependency'   => array( 'bppiv_type', '==', 'image' ),
    ),

    // Video Settings
    array(
      'id'       => 'bppiv_auto_play',
      'type'     => 'switcher',
      'title'    => esc_html__('Auto Play ?', 'panorama-viewer'),
      'desc'     => esc_html__('Enable or Disable Auto Play', 'panorama-viewer'),
      'text_on'  => 'Yes',
      'text_off' => 'No',
      'default'  => true,
      'dependency'   => array( 'bppiv_type', '==', 'video' ),
    ),
    array(
      'id'       => 'bppiv_video_mute',
      'type'     => 'switcher',
      'title'    => esc_html__('Video Mute ?', 'panorama-viewer'),
      'subtitle' => esc_html__('Enable or Disable Video Mute', 'panorama-viewer'),
      'desc'     => esc_html__('Specify if the video should auto play', 'panorama-viewer'),
      'text_on'  => 'Yes',
      'text_off' => 'No',
      'default'  => true,
      'dependency'   => array( 'bppiv_type', '==', 'video' ),
    ),
    array(
      'id'       => 'bppiv_video_loop',
      'type'     => 'switcher',
      'title'    => esc_html__('Video Loop ?', 'panorama-viewer'),
      'desc'     => esc_html__('Enable or Disable Video Loop', 'panorama-viewer'),
      'text_on'  => 'Yes',
      'text_off' => 'No',
      'default'  => true,
      'dependency'   => array( 'bppiv_type', '==', 'video' ),
    ),
    array(
      'id'       => 'control_show_hide_video',
      'type'     => 'switcher',
      'title'    => esc_html__('Hide Control Bar ?', 'panorama-viewer'),
      'desc'     => esc_html__('Show or Hide Control', 'panorama-viewer'),
      'text_on'  => 'Yes',
      'text_off' => 'No',
      'default'  => false,
      'dependency'   => array( 'bppiv_type', '==', 'video' ),
    ),
    
  ) // End fields


) );
